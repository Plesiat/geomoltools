
####################
Tutorial
####################

In this tutorial, we are going to show how to use the different codes from the package geomoltools in order to manipulate (rotate, translate, join, pack, convert, etc.) molecular geometries.

Before starting with our practical example, one has to be sure that the 8 codes have been compiled correctly and that the path of the directory containing the executables is exported (e.g, to a variable named "PROGDIR").


We begin with two .mol files: polymer-1.mol and pcbm.mol. These two files contain the cartesian coordinates and the bond specifications of two organic molecules: the 4-[5-(2,1,3-benzothiadiazol-4-yl)thiophen-2-yl]-7-(thiophen-2-yl)-2,1,3-benzothiadiazole (polymer-1) and the Phenyl-C61-butyric acid methyl ester (PCBM).

mol2xyz
_______

To be able to work with the geometries of the two molecules, we will first convert each file in the .xyz format:

:: 

    $PROGDIR/mol2xyz.exe < mol2xyz_1.inp
    $PROGDIR/mol2xyz.exe < mol2xyz_2.inp

The resulting .xyz files contained the cartesian coordinates of each molecule, ordered according to the given initial atom index and the molecular bonds.

pastemol
________

By using *pastemol*, it is possible to gather these geometries into a single .xyz file:

:: 

  $PROGDIR/pastemol.exe < pastemol_1.inp

When visualizing (e.g., using the software avogadro) the resulting .xyz file (polymer-1_r_pcbm_r.xyz), we realize that the two molecules are mixed and that it is not possible to distinguish them clearly. To have a clearer view of the two molecules together, we should first rotate and translate one of the molecules (in this case PCBM) and join the two molecules together again.


movemol
_______

The rotation/translation can be performed using *movemol* which permits to translate and rotate (align) a molecule by defining 3 centroids (centers define by a group of atoms) and 3 vectors. The first centroid is defined by giving a first set of atom indices and correspond to the point translated to the position defined by the coordinates (pos0). The main axis of the molecule is defined by setting a second centroid. This axis (centroid1-centroid2) is rotated to be aligned with the first alignment vector (vec1). A second rotation can be performed by setting a third centroid (not aligned with the previously defined axis) and a second alignement vector (vec2):

:: 

  $PROGDIR/movemol.exe < movemol.inp

With the parameters contained in movemol.inp, we have produced a new .xyz file (pcbm_r_a.xyz) which contained the rotated/translated PCBM molecule. By using a visualizer, we can observe that:

* the coordinates of the atom 1 is (0,0,10)
* atoms 87 and 88 are in the plane (x,z)
* the midpoint is located at (x=0,y=0)

If we now join the geometry of polymer-1 and the aligned one of pcbm with *pastemol* , it is then possible to identify clearly the two molecules:

:: 

  $PROGDIR/pastemol.exe < pastemol_2.inp


stackmol (manual)
_________________

The program stackmol allows to automatize the previous process in order to generate a great variety of geometries in which a molecule is orientated in different ways with respect to another according to predefined criteria. Two modes can be used in order to generate these geometries: the manual and the random one. Let's start first by the manual mode.

The input file stackmol_1.inp contains the name of the two .xyz files corresponding respectively to the fixed and the mobile geometry and the name of a file including the parameters for the generation of geometries in the manual mode. In the manual mode, one has to define by hand the three centroids (in the same logic as before, when using *movemol*) and the grid of translation vectors and rotation angles (we are using here rotation angles instead of alignment vectors).

:: 

  $PROGDIR/stackmol.exe < stackmol_1.inp

It generates 24 geometries (polymer-1_r_pcbm_r_a_mp-manual_0000%i.xyz) and a .XYZ file which gather all the geometries in one file. By using a visualizer, it is possible to notice the following points:

* atom 37 (= atom 1 for the isolated PCBM) is located at the origin for the first 12 geometries and at the center of the first aromatic ring of the polymer for the rest of the geometries
* the PCBM is shifted by a constant value (6 Ang) in the z-direction in all the cases
* in every geometry, atom 1 (= atom 1 for the isolated polymer), atom 37 (= atom 1 for the isolated PCBM) and atoms 123 and 124 (= atoms 87 and 88 for the isolated PCBM) belong to a plane which rotates by phi2=Pi/4 with respect to the axis defined by the atom 37 and the centroid of atoms 123 and 124
* this axis is parallel to (x,y) plane (when theta1=Pi/2) for geometries 1-6 and 13-18, and aligned with z-axis (when theta1=Pi) for geometries 7-12 and 19-24
* this axis is rotated by phi1=45 every 3 geometries
* it is important to notice that the plane defined by the atom 1-37-123-124 is brought back to its initial condition ( i.e., perpendicular to the (x, y) plane) every times that phi1 changes. This is possible because we have chosen a particular initial orientation of the PCBM molecule (the main axis being aligned with the z-axis). Different orientations would be obtained when using the geometry pcbm_r.xyz.

We want now to generate the same geometries but adding some selection criteria. It is possible to define these criteria by using different criteria modes, but we will focus here on the most obvious one: we want to set a lower and an upper limit for the minimum and maximum distance between two nearest atoms from each molecule. This is obtained by adding at the end of the parameter file mp-manual.mdp the values "rlim" (criteria mode), 2 (lower limit) and 15 (upper limit).

:: 

  $PROGDIR/stackmol.exe < stackmol_2.inp

It generates 12 geometries only, from 1-6 and 13-18. This is due to the fact that only the cases where the main axis of the PCBM is parallel to the (x,y) plane respect the given criteria (the minimum distance between two atoms from each molecule in the perpendicular case being about 1.8 Ang < 2 Ang).


geodiff
_______

By using the program *geodiff*, we can control that the 12 produced geometries are the same than the 12 previous ones. For example, let's check that the first geometries obtained with and without selection criteria are identical:

:: 

  $PROGDIR/geodiff.exe < geodiff_1.inp

Of course, if we now compare the first with the second geometry, *geodiff* shows that the two geometries are different:

:: 

  $PROGDIR/geodiff.exe < geodiff_2.inp

By setting the verbose option to 1, it is possible to have a more detailed summary of the differences between both geometries:

:: 

  $PROGDIR/geodiff.exe < geodiff_3.inp

  
stackmol (random)
_________________
  
With stackmol, it is also possible to produce geometries in which the orientations of the second molecule are generated (pseudo-)randomly. 
This option is enabled by setting the generation mode option to "random". The parameters required are the number of geometries to be produced, a tolerance parameter in Ang/atom and a lower and upper limit for the minimum and maximum distance between two atoms from each molecule.

By using the inputfile mp-random-2val.mdp, we will obtained 10 geometries fulfilling the required criteria:

:: 

  $PROGDIR/stackmol.exe < stackmol_3.inp

By repeating the same calculation, we will obtain 10 new and different geometries fulfilling the same criteria. We can control that the new set of geometries is different from the previous one by using *geodiff*:

:: 

  $PROGDIR/geodiff.exe < geodiff_4.inp

When setting the upper limit equals to the lower limit, we obtain geometries in which the minimum distance between two atoms from each molecule is strictly equals to the lower limit (in the range defined by the tolerance):

:: 

  $PROGDIR/stackmol.exe < stackmol_4.inp

In this particular case, we have used the verbose option equals to 2 in order to generate 4 additional output files with suffix *_rval-min.out, *_rval-max.out, *_dist-min.out and *_dist-max.out.
They contain respectively the values of the minimum distance, the maximum distance, the spatial coordinates of the atom of geometry2 which is the closest to atom of geometry1 and the spatial coordinates of the atom of geometry2 which is the farthest from its nearest atom from geometry1. Each line of these files corresponds to a different accepted geometry.


xyz2zmt_s (standard)
____________________

If we need to convert the cartesian coordinates of the geometries into Z-matrix, we can use *xyz2zmt_s*.

A file containing the Z-matrix in a standard format can be obtained by setting the "nspecies" parameter to 0. In such case, the atomic labels from the .xyz file will be used and the Z-matrix will be generated by establishing the array of atom indices defining the internal coordinates with respect to the previously defined nearest atoms.

:: 

  $PROGDIR/xyz2zmt_s.exe < xyz2zmt_1.inp > polymer-1_r_pcbm_r_a_mp-manual_000001.zms

The resulting .zms file contains the Z-matrix of the first geometry. 


zmt2xyz_s
____________________

By using *zmt2xyz_s*, this .zms file can be converted back to cartesian coordinates and written in a .xyzfile.

:: 

  $PROGDIR/zmt2xyz_s.exe < zmt2xyz_1.inp > polymer-1_r_pcbm_r_a_mp-manual_000001_new.xyz

The new .xyz file is different to the original one because information about the orientation of the molecule in the laboratory frame has been lost during the conversion of the cartesian coordinates into internal coordinates. However, the two geometries are identical, as shown by using *geodiff*:

:: 

  $PROGDIR/geodiff.exe < geodiff_5.inp


xyz2zmt_s (Siesta)
__________________

It is also possible to convert the cartesian coordinates into a Z-matrix readable by Siesta. In such case, the parameter "nspecies" must be equal to the number of different species appearing in the .xyz file and the label of each atom must be included to the input file in the increasing order of the specie index (as defined in Siesta input file).

Siesta allows to freeze or release the internal coordinates of each atom by respectively adding after each line of the Z-matrix a set of 3 numbers (being 0 or 1) for each corresponding internal coordinate. These options can be controlled from the input file of xyz2zmt by giving 3 numbers (nmov) corresponding t the number of atoms to be released for each type of internal coordinate (bond, bond angle and torsion). The 3 lists of atom indices (amov) corresponding to each type of internal coordinates is read afterward. If nmov=0 for a specific type of internal coordinate, then no atom indices will be read and all the atom will be frozen for this type of internal coordinate. If nmov=1 and amov=0 for a specific type of internal coordinate, then all the atom will be released for this type of internal coordinate.
By setting "ncut" and "nsplit" different from 0, we can define the atom indices for which we want to create sections in the Z-matrix. The difference of "ncut" compared to "nsplit" is that it add the required freezing/release options so the defined section can move relatively with respect to the previous one, as illustrated in the following example:

:: 

  $PROGDIR/xyz2zmt_s.exe < xyz2zmt_2.inp > polymer-1_r_pcbm_r_a_mp-manual_000001.zmt

We can convert back the resulting .zmt file to a .xyz file:

:: 

  $PROGDIR/zmt2xyz_s.exe < zmt2xyz_2.inp > polymer-1_r_pcbm_r_a_mp-manual_000001_new2.xyz

This file can be directly compared to polymer-1_r_pcbm_r_a_mp-manual_000001_new.xyz and should be identical.


ucubcellgen
___________

After converting cartesian coordinates to Z-matrix or after rotating cartesian coordinates, we may need to redefine the unit cell (in this case cubic) depending on how the molecules have to be periodically bounded and orientated to each others. For this purpose, we can use *ucubcellgen* that allows us to define the new vx vector of the unit cell by giving the index of a referent atom "ind0" (in an adjacent cell) and a set of 3 atom indices (in the unit cell) with the corresponding bond length, bond angle and dihedral angle values. Once vx is calculated, the new vy vector will be obtained simply by taking a referent atom "ind1" (in the adjacent cell) and the index of an atom "ind2" (in the unit cell) with their respective distance (dist1). The vz vector is calculated directly from vx and vy and only the length (dist2) of the unit cell in this direction is required.

In this example, we build the polymer chain and we define ind1=ind2=2 so that all the atoms of the polymer are in the plane (x,y):

:: 

  $PROGDIR/ucubcellgen.exe < ucubcellgen_1.inp

The program prints the new unit cell vectors, as well as it produces a .xyz supc file containing the original molecule and its replicas in three adjacent cells (in the x, y and z direction). This file permits to visualize how the molecules will be oriented with the new unit cell. Make a backup of the file "polymer-1_r_pcbm_r_a_mp-manual_000001_supc.xyz":

:: 

  cp polymer-1_r_pcbm_r_a_mp-manual_000001_supc.xyz polymer-1_r_pcbm_r_a_mp-manual_000001_supc_0.xyz

The same result can be obtained from the .xyz file, as shown by:

:: 

  $PROGDIR/ucubcellgen.exe < ucubcellgen_2.inp

The program *geodiff* confirms that the two produced supc files are the same:

:: 

  $PROGDIR/geodiff.exe < geodiff_6.inp

.. It is also possible to choose two different atom indices for the definition of the vy vector of the new unit cell. For example, we choose two atoms of the PCBM which are not aligned with respect to any x, y or z directions (73 and 86):
..
.. :: 
..
..  $PROGDIR/ucubcellgen.exe < ucubcellgen_3.inp
..
.. This produces a new supc file (here named with the suffix *_new2.xyz) where the molecules are tilted with respect to each other. When looking to the atoms 73 and 86, we can see that these atoms have the same value in z and that the distance between atom 86 and atom 334 (atom 73 of the molecule in the cell replicated in the y direction) is equal to the required value (dist1).

Given the unit cell vector, *stackmol* has the possibility to take into account the periodicity of the crystal in the random mode:

:: 

  $PROGDIR/stackmol.exe < stackmol_5.inp

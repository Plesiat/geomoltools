
!> pastemol: program pastemol
!> pastemol: Depends on getbasename, getextension, checkext, numaxyzfile, numamolfile, readxyzfile, readmolfile, stacktest, joinmol, writexyzfile, writemolfile
!> pastemol: Gather the two geometries contained in the .xyz/.mol files molfile1 and molfile2 in a new single .xyz/.mol file with or without selection criteria (modtrs and valtrs).
  program pastemol

  implicit none

  integer i, iuf, iuf2(4)
  integer natm1, natm2, natm3
  integer nbnd1, nbnd2, nbnd3
  integer verbose
  integer fmode
  character*6 modtrs
  double precision valtrs(2)
  double precision, dimension (:,:), allocatable :: cmt1, cmt2, cmt3
  integer, dimension (:,:), allocatable :: bnd1, bnd2, bnd3
  character*2, dimension (:), allocatable :: atm1, atm2, atm3
  character*150 molfile1, molfile2, rootname, outname, getbasename, getextension
  integer ok
  logical gtest
  
    read(*,'(a)') molfile1
    read(*,'(a)') molfile2
    read(*,'(a)',iostat=ok) verbose
    if ( ok == 0 ) read(*,*,iostat=ok) modtrs
    if ( ok == 0 .and. modtrs /= "" .and. modtrs /= "none" ) then
      do i=1, 2
	read(*,*) valtrs(i)
      enddo
    endif

  call checkext(trim(molfile1),2,(/"xyz","mol"/))
  call checkext(trim(molfile2),2,(/"xyz","mol"/))
  
  rootname=trim(getbasename(molfile1))//"_"//trim(getbasename(molfile2))
  
  select case (trim(getextension(molfile1)))
  case("xyz")
    if ( trim(getextension(molfile2)) == "xyz" ) then
      fmode=0
    else
      fmode=-1
    endif
  case("mol")
    if ( trim(getextension(molfile2)) == "mol" ) then
      fmode=1
    else
      fmode=-1
    endif
  end select
  
  if ( fmode == -1 ) then
    write(*,*)
    write(*,*) "Error!!! Extension of molfile1 and molfile2 should match!"
    stop
  endif
  
  select case (fmode)
  case(0)
    outname=trim(rootname)//".xyz"
    call numaxyzfile(trim(molfile1),natm1)
    call numaxyzfile(trim(molfile2),natm2)

    allocate(cmt1(natm1,3),atm1(natm1))
    allocate(cmt2(natm2,3),atm2(natm2))

    call readxyzfile(trim(molfile1),atm1,cmt1,natm1)
    call readxyzfile(trim(molfile2),atm2,cmt2,natm2)
    
  case(1)
    outname=trim(rootname)//".mol"
    call numamolfile(trim(molfile1),natm1,nbnd1)
    call numamolfile(trim(molfile2),natm2,nbnd2)

    allocate(cmt1(natm1,3),atm1(natm1))
    allocate(cmt2(natm2,3),atm2(natm2))
    allocate(bnd1(nbnd1,3))
    allocate(bnd2(nbnd2,3))

    call readmolfile(trim(molfile1),cmt1,atm1,bnd1,natm1,nbnd1)
    call readmolfile(trim(molfile2),cmt2,atm2,bnd2,natm2,nbnd2)
    nbnd3=nbnd1+nbnd2
    allocate(bnd3(nbnd3,3))
    bnd3(1:nbnd1,:)=bnd1(:,:)
    bnd3(nbnd1+1:nbnd3,1:2)=bnd2(:,1:2)+natm1
    bnd3(nbnd1+1:nbnd3,3)=bnd2(:,3)
    
  end select

  natm3=natm1+natm2
  allocate(cmt3(natm3,3),atm3(natm3))
  
  iuf2=0
  if ( verbose == 2) then
    open(newunit=iuf2(1),file=trim(rootname)//"_dist-min.out")
    open(newunit=iuf2(2),file=trim(rootname)//"_rval-min.out")
    open(newunit=iuf2(3),file=trim(rootname)//"_dist-max.out")
    open(newunit=iuf2(4),file=trim(rootname)//"_rval-max.out")
  endif
  call stacktest(cmt1,natm1,cmt2,natm2,modtrs,valtrs,verbose,gtest)
  call joinmol(gtest,cmt1,atm1,natm1,cmt2,atm2,natm2,verbose,iuf2,cmt3,atm3)
  if ( verbose == 2) then
    close(iuf2(1))
    close(iuf2(2))
    close(iuf2(3))
    close(iuf2(4))
  endif
  
  if ( gtest ) then
    select case (fmode)
    case(0)
      call writexyzfile(trim(outname),atm3,cmt3,natm3)
    case(1)
      call writemolfile(trim(outname),atm3,cmt3,bnd3,natm3,nbnd3)
      deallocate(bnd1,bnd2,bnd3)
    end select
  else
    write(*,*)
    write(*,*) "Error!!! Program failed to paste the two geometries according to the modtrs criteria!"
    write(*,*)
  endif
  
  deallocate(cmt1,atm1)
  deallocate(cmt2,atm2)
  deallocate(cmt3,atm3)

  end

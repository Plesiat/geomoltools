!> stackmol: program stackmol
!> stackmol: Depends on getbasename, ang2vec, checkext, numaxyzfile, readxyzfile, sequify, getgrid, nb2car_zpad, alignmol, stacktest, joinmol, writexyzfile, writemultixyzfile, maxdistcoo, initrandseed, checkfile
!> stackmol: Generates and write in the .xyz format a set of different geometries containing the cartesian coordinates of two geometries read from xyzfile1 and xyzfile2 and pasted together.
!> stackmol: Each written .xyz file corresponds to a geometry with a particular orientation of geometry2 with respect to geometry1, depending on the process mode option (pmode) and its related parameters.
!> stackmol: pmode can take the following values: "manual" and "random"
!> stackmol: pmode="manual" corresponds to a mode in which the orientations of geometry2 are defined manually by a set of parameters contained in the inputfile
!> stackmol: pmode="random" corresponds to a mode in which the orientations of geometry2 are generated using a (pseudo-)random number generator and are selected according to parameters defined in the inputfile

  program stackmol

  use vecop

  implicit none

  integer i, j, k, l, m
  integer iuf, iuf2(4)
  character*150 xyzfile1, xyzfile2, movparfile, rootname, outname, multiname
  character*150 getbasename
  character*6 modtrs
  integer modcell
  double precision valtrs(2), vucell(3,3)
  integer verbose
  integer natm1, natm1b, natm2, natm3, natm5
  double precision, dimension (:,:), allocatable :: cmt1a, cmt1b, cmt2a, cmt2b, cmt3, cmt4, cmt5
  character*2, dimension (:), allocatable :: atm1a, atm1b, atm2, atm3, atm4, atm5
  integer itrs, itet1, iphi1, itet2, iphi2
  integer ntrs, ntet1, nphi1, nphi2
  integer ngrid
  double precision toldiff, avgdiff, Lbox
  integer numfldstr
  double precision trg(3)
  double precision, dimension (:,:), allocatable :: trsgrid
  double precision, dimension (:), allocatable :: tet1grid, phi1grid, phi2grid
  integer ninda(3)
  integer, dimension (:,:), allocatable :: inda
  double precision d_tmp(3), cat0(3), align1(3), align2(3)
  character*150 pmode, gmode, s_tmp
  logical gtest, dtest
  integer ok
  integer nz
  character*50 sind
  character*15 FMT1, FMT2
  parameter (nz=6,FMT1='(1000i6)',FMT2='(i6,1000f8.2)')
  double precision xyzmin(3), xyzmax(3)
  integer klim, ktd
  parameter (klim=100000)

  !% Reads the filename of the three inputfiles
    read(*,'(a)') xyzfile1
    read(*,'(a)') xyzfile2
    read(*,'(a)') movparfile
    read(*,*) verbose

  !% Checks the extensions of the three inputfiles
  call checkext(trim(xyzfile1),1,"xyz")
  call checkext(trim(xyzfile2),1,"xyz")
  call checkext(trim(movparfile),1,"mdp")

  rootname=trim(getbasename(xyzfile1))//"_"//trim(getbasename(xyzfile2))//"_"//trim(getbasename(movparfile))
  multiname=trim(rootname)//".XYZ"

  !% Counts the number of lines of xyzfile1 and xyzfile2. Reads and stores their values in (atm1a,cmt1a) and (atm2,zmt2) respectively.
  call numaxyzfile(trim(xyzfile1),natm1)
  allocate(cmt1a(natm1,3), atm1a(natm1))
  call readxyzfile(trim(xyzfile1),atm1a,cmt1a,natm1)
  call numaxyzfile(trim(xyzfile2),natm2)
  allocate(cmt2a(natm2,3), cmt2b(natm2,3), atm2(natm2))
  call readxyzfile(trim(xyzfile2),atm2,cmt2a,natm2)

  !% Reads the parameters contained in the file defining the orientation of geometry2
  open(newunit=iuf,file=trim(movparfile))
    read(iuf,*) pmode

    select case (trim(pmode))
    case ("manual")
      !% If the generation is manual, we need to read the indices of the first centroid (inda(:,1)), the coordinates of a constant translation vector (trg), a set of translations vectors (trsgrid), 
      !% the indices of the second centroid (inda(:,2)), a set of polar angles (tet1grid) in degree for the first rotation, a set of azimuthal angles (phi1grid) in degree for the first rotation,
      !% the indices of the third centroid (inda(:,3)), a set of azimuthal angles (phi2grid) in degree for the second rotation,
      !% the criterium mode (modtrs) used in the subroutine joinmol and the corresponding distance values valtrs(1) and valtrs(2).
      allocate(inda(natm2,3))

      read(iuf,'(a)') s_tmp
      call sequify(trim(s_tmp),natm2,inda(:,1),ninda(1))

      do i=1, 3
	read(iuf,*) trg(i)
      enddo

      read(iuf,*) gmode, ntrs
      allocate(trsgrid(ntrs,3))
      call getgrid(iuf,gmode,ntrs,3,trsgrid)

      read(iuf,'(a)') s_tmp
      call sequify(trim(s_tmp),natm2,inda(:,2),ninda(2))

      read(iuf,*) gmode, ntet1
      allocate(tet1grid(ntet1))
      call getgrid(iuf,gmode,ntet1,1,tet1grid)
      read(iuf,*) gmode, nphi1
      allocate(phi1grid(nphi1))
      call getgrid(iuf,gmode,nphi1,1,phi1grid)

      read(iuf,'(a)') s_tmp
      call sequify(trim(s_tmp),natm2,inda(:,3),ninda(3))

      read(iuf,*,iostat=ok) gmode, nphi2
      allocate(phi2grid(nphi2))
      call getgrid(iuf,gmode,nphi2,1,phi2grid)
      
      if ( ok == 0 ) read(iuf,*,iostat=ok) modtrs
      if ( ok == 0 .and. modtrs /= "" .and. modtrs /= "none" ) then
	do i=1, 2
	  read(iuf,*) valtrs(i)
	enddo
      endif
    
    case ("random")
      
      !% If the generation is made using the (pseudo-)random number generator, define the three centroid (inda) as the first, the second and the third atom of geometry2 respectively,
      !% reads the number of geometries to be generated (ngrid), reads the tolerance in terms of average difference (Ang/atom) (i.e., if the average difference between two generated geometries
      !% is less than toldiff, do not accept the newly generated geometry), reads the lower and higher limits for the modtrs="rlim" mode.
      allocate(inda(1,3))
      do i=1, 3
	inda(1,i)=i
      enddo
      read(iuf,*) ngrid
      read(iuf,*) toldiff
      do i=1, 2
	read(iuf,*) valtrs(i)
      enddo
      modtrs="rlim"
      read(iuf,*) modcell
      if ( modcell == 1 ) then
	do i=1, 3
	  read(iuf,*) (vucell(i,j), j=1, 3)
	enddo
      endif

    case default

      write(*,*)
      write(*,*) "Error!!! Bad argument for option pmode: pmode=", pmode
      write(*,*)
      stop

    end select
  close(iuf)

  natm3=natm1+natm2
  allocate(cmt3(natm3,3),atm3(natm3))

  !% The multi-xyzfile needs to be opened before the loop over the geometries
  open(newunit=iuf,file=trim(multiname))
  if ( verbose == 2) then
    open(newunit=iuf2(1),file=trim(rootname)//"_dist-min.out")
    open(newunit=iuf2(2),file=trim(rootname)//"_rval-min.out")
    open(newunit=iuf2(3),file=trim(rootname)//"_dist-max.out")
    open(newunit=iuf2(4),file=trim(rootname)//"_rval-max.out")
  endif
  
  select case (trim(pmode))
  case ("manual")
  
    !% Calculate the number of elements in the grid
    ngrid=ntrs*ntet1*nphi1*nphi2
    !% Prints the details of the grid
    if ( verbose > 0 ) then
      write(*,'(a,i6)') "Number of elements in the grid: ", ngrid
      write(*,FMT1) (inda(i,1), i=1, ninda(1))
      do i=1, 3
	write(*,FMT2) ntrs, (trsgrid(j,i), j=1,ntrs)
      enddo
      write(*,FMT1) (inda(i,2), i=1, ninda(2))
      write(*,FMT2) ntet1, (tet1grid(i), i=1,ntet1)
      write(*,FMT2) nphi1, (phi1grid(i), i=1,nphi1)
      write(*,FMT1) (inda(i,3), i=1, ninda(3))
      write(*,FMT2) nphi2, (phi2grid(i), i=1,nphi2)
      write(*,*)
    endif
    
    !% Loop ver all the geometries defined by the grid
    k=1
    do itrs=1, ntrs
      !% We add the constant translation vector to every element of trsgrid
      cat0=trg+trsgrid(itrs,:)
      do itet1=1, ntet1
	do iphi1=1, nphi1
	  !% The polar angle tet1grid(itet1) and the azimuthal angle phi1grid(iphi1) are converted in a normalized vector align1
	  do iphi2=1, nphi2
	  
	    !% cmt2a is copied because alignmol will rotate/translate it
	    cmt2b=cmt2a
	    
	    call nb2car_zpad(k,nz,sind)
	    outname=trim(rootname)//"_"//trim(sind)//".xyz"

	    if ( verbose > 0 ) write(*,'(i6,a,i6,2X,a,2X,6f10.4)') k, "/", ngrid, trim(outname), &
	    cat0, tet1grid(itet1), phi1grid(iphi1), phi2grid(iphi2)
	    
	    !% Rotate and translate cmt2 according to the current values of translation vector cat0 and alignements vectors align1 and align2.
	    call alignmol(natm2,cmt2b,ninda(1),inda(:,1),cat0,ninda(2),inda(:,2),(/tet1grid(itet1),phi1grid(iphi1)/),&
	                  ninda(3),inda(:,3),(/phi2grid(iphi2)/),0,1)
	    !% Join cmt1a with the resulting transformed array of cartesian coordinates cmt2 according to the criteria defined by the criterium mode option (modtrs) and the two limiting values valtrs.
	    call stacktest(cmt1a,natm1,cmt2b,natm2,modtrs,valtrs,verbose,gtest)
	    call joinmol(gtest,cmt1a,atm1a,natm1,cmt2b,atm2,natm2,verbose,iuf2,cmt3,atm3)
	    !% If the criteria for joining the two arrays of cartesian coordinates have been fulfilled, then write the resulting gathered geometry in an outputfile with geometry index k
	    !% and add it to the multi-xyzfile
	    if ( gtest ) then
	      call writexyzfile(trim(outname),atm3,cmt3,natm3)
	      call writemultixyzfile(iuf,atm3,cmt3,natm3)
	    else
	      if ( verbose > 0 ) write(*,*) "Warning! The following geometry has been discarded: ", k
	    endif
	    k=k+1
	  enddo
	enddo
      enddo
    enddo
  
    deallocate(trsgrid)
    deallocate(tet1grid,phi1grid)
    deallocate(phi2grid)
  
  case("random")
  
    allocate(cmt4(natm3,3), atm4(natm3))
    !% If we use a cubic unit cell, then take the minimum length value
    if ( modcell == 1 ) then
      do i=1, 3
	d_tmp(i)=normvec(vucell(i,:))
      enddo
      Lbox=minval(d_tmp)
      write(*,*)
      write(*,'(a,f8.4)') "Lbox has been calculated automatically from the defined cubic unit cell: Lbox=", Lbox
      
      !% If want to set up a unit cell, we then have to take into a account the replica of the first molecule
      natm1b=27*natm1
      allocate(cmt1b(natm1b,3),atm1b(natm1b))
      l=1
      do i=-1, 1
	do j=-1, 1
	  do k=-1, 1
	    do m=1, natm1
	      cmt1b((l-1)*natm1+m,:)=cmt1a(m,:)+i*vucell(1,:)+j*vucell(2,:)+k*vucell(3,:)
	    enddo
	    l=l+1
	  enddo
	enddo
      enddo
      do i=1, 27
	atm1b((i-1)*natm1+1:i*natm1)=atm1a(1:natm1)
      enddo
      
      natm5=natm1b+natm2
      allocate(cmt5(natm5,3), atm5(natm5))
      
    else
      !% If the two limiting values are equal, then set the maximal distance between two nearest atoms belonging to the different geometries as equal to the maximal distance between two atoms in the geometry2
      if ( valtrs(1) == valtrs(2) ) then
	call maxdistcoo(cmt2a,natm2,Lbox)
	Lbox=Lbox+valtrs(1)
	write(*,*)
	write(*,*) "Lbox has been calculated automatically form the maximum distance between two atoms in molecule 2: Lbox=", Lbox
      else
	Lbox=valtrs(2)
      endif
      
      !% If not modcell, consider only the isolated molecule 1
      natm1b=natm1
      allocate(cmt1b(natm1b,3),atm1b(natm1b))
      cmt1b=cmt1a
      atm1b=atm1a
    endif


    !% Calculate the offset to be add in each direction in order to respect the maximal distance criterium
    do i=1, 3
      xyzmin(i)=minval(cmt1a(:,i))-Lbox
      xyzmax(i)=maxval(cmt1a(:,i))+Lbox
    enddo
    

    i=0
    k=0
    ktd=0
    !% Initialize the seed for the random number generation
    call initrandseed
    do while ( k < ngrid .and. k < klim )
    
      !% cmt2a is copied because alignmol will rotate/translate it
      cmt2b=cmt2a
    
      !% Generate 3 sets of 3 random number in the correct range of values for the translation vector cat0 and the two alignement vectors align1 and align2 respectively
      call random_number(cat0)
      cat0=cat0*(xyzmax-xyzmin)+xyzmin
      call random_number(align1)
      align1=align1*2.d0-1.d0
      call random_number(align2)
      align2=align2*2.d0-1.d0
      
      !% Rotate and translate cmt2 according to the current values of translation vector cat0 and alignements vectors align1 and align2.
      call alignmol(natm2,cmt2b,1,inda(1,1),cat0,1,inda(1,2),align1,1,inda(1,3),align2,0,0)
      !% Join cmt1a with the resulting transformed array of cartesian coordinates cmt2 according to the criteria defined by the criterium mode option (modtrs) and the two limiting values valtrs.
      call stacktest(cmt1a,natm1,cmt2b,natm2,modtrs,valtrs,verbose,gtest)
      if ( gtest .and. modcell == 1 ) call stacktest(cmt1b,natm1b,cmt2b,natm2,modtrs,valtrs,verbose,gtest)
      
      !% If the criteria for joining the two arrays of cartesian coordinates have been fulfilled, checks that the new geometry is not a doublon by comparating it with the cartesian coordinates 
      !% contained in the previously generated .xyz files and checking that the average difference is higher than a tolerance number (toldiff).
      !% If this criterium is fulfilled, then write the resulting gathered geometry in an outputfile with geometry index k and add it to the multi-xyzfile.
      if ( gtest ) then
	dtest=.true.
	if ( toldiff /= 0.d0 ) then
	  do j=1, k
	    call nb2car_zpad(j,nz,sind)
	    xyzfile1=trim(rootname)//"_"//trim(sind)//".xyz"
	    !% Checks that the file xyzfile1 previously written exists
	    call checkfile(trim(xyzfile1))
	    call readxyzfile(trim(xyzfile1),atm4,cmt4,natm3)
	    !% Calculates the average difference (avgdiff) between the new geometry cmt2 and the old one cmt4
	    call avgdiffcoo(cmt4(natm1+1:natm3,:),cmt2b,natm2,avgdiff)
	    if ( avgdiff < toldiff ) then
	      dtest=.false.
	      ktd=ktd+1
	      exit
	    endif
	  enddo
	endif
	!% If this last criterium is fulfilled, write the atom labels (atm3) and the cartesian coordinates (cmt3) of the new geometry in a .xyz file and multi-xyzfile.
	if ( dtest ) then
	  
	  !% If all the criteria are respected, join the two molecules
	  call joinmol(gtest,cmt1a,atm1a,natm1,cmt2b,atm2,natm2,verbose,iuf2,cmt3,atm3)
	  
	  !% Write the individual .xyz file
	  k=k+1
	  call nb2car_zpad(k,nz,sind)
	  outname=trim(rootname)//"_"//trim(sind)//".xyz"
	  if ( verbose > 0 ) write(*,'(i6,a,i6,2X,a,2X,9f10.4)') k, "/", ngrid, trim(outname), cat0, align1, align2
	  call writexyzfile(trim(outname),atm3,cmt3,natm3)
	  
	  !% Complete the multi-xyzfile
	  if ( modcell == 1 ) then
	    call joinmol(gtest,cmt1b,atm1b,natm1b,cmt2b,atm2,natm2,verbose,iuf2,cmt5,atm5)
	    call writemultixyzfile(iuf,atm5,cmt5,natm5)
	  else
	    call writemultixyzfile(iuf,atm3,cmt3,natm3)
	  endif
	  
	endif
      endif
      
      i=i+1
    enddo
    if ( verbose > 0 ) write(*,*) "Accepted, m-discarded, d-discarded, total produced geometries: ", k, i-k-ktd, ktd, i
    
    deallocate(cmt4,atm4)
    deallocate(cmt1b,atm1b)
    if ( modcell == 1 ) deallocate(cmt5,atm5)
    
  end select

  close(iuf)
  if ( verbose == 2) then
    close(iuf2(1))
    close(iuf2(2))
    close(iuf2(3))
    close(iuf2(4))
  endif

  deallocate(cmt1a,atm1a)
  deallocate(cmt2a,cmt2b,atm2)
  deallocate(cmt3,atm3)
  deallocate(inda)

  end program stackmol


!> gridgen: subroutine gridgen(gmin,gstep,nstep,vgrid)
!> gridgen: Generates a grid of values (vgrid) according to the minimum value of the grid (gmin), the step value (gstep) and the number of elements in the grid (nstep).
  subroutine gridgen(gmin,gstep,nstep,vgrid)

    implicit none

    double precision, intent(in) :: gmin, gstep
    integer, intent(in) :: nstep
    double precision, intent(out) :: vgrid(nstep)

    integer i

    do i=1, nstep
      vgrid(i)=dble(i-1)*gstep+gmin
    enddo

    return

  end subroutine gridgen


!> getgrid: subroutine getgrid(iuf,gmode,nstep,nc,vgrid)
!> getgrid: Depends on gridgen
!> getgrid: Generates a grid of values (vgrid) depending on the grid mode option (gmode), the number of step of the grid (nstep) and the number of values per grid point (nc).
!> getgrid: gmode can takes 2 values: "Q:" and "G:" 
!> getgrid: gmode="Q:" corresponds to the automatic generation mode. It reads the minimum value of the grid (gmin) and the step value of the grid (gstep) and generates the corresponding grid by calling gridgen
!> getgrid: gmode="G:" corresponds to the manual mode. The values of the grid are read directly from the file.
  subroutine getgrid(iuf,gmode,nstep,nc,vgrid)

    implicit none

    integer, intent(in) :: iuf
    character*(*), intent(in) :: gmode
    integer, intent(in) :: nstep, nc
    double precision, intent(out) :: vgrid(nstep,nc)

    integer i, j
    double precision gmin, gstep

    if ( nstep <= 0 ) then
      write(*,*)
      write(*,*) "Error!!! Bad number of grid step: nstep=", nstep
      write(*,*)
      stop
    endif

    select case (trim(gmode))
    case("Q:")
      do i=1, nc
	read(iuf,*) gmin, gstep
	call gridgen(gmin,gstep,nstep,vgrid(:,i))
      enddo

    case("G:")
      do i=1, nc
	read(iuf,*) (vgrid(j,i), j=1, nstep)
      enddo

    case default
      write(*,*)
      write(*,*) "Error!!! Bad argument for option gmode: gmode=", gmode
      write(*,*)
      stop

    end select

    return

  end subroutine getgrid

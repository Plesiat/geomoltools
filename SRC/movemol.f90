!> movemol: program movemol
!> movemol: Depends on getextension, getbasename, checkext, numaxyzfile, numamolfile, readxyzfile, readmolfile, alignmol, writexyzfile, writemolfile
!> movemol: Translates and aligns a geometry defined by 2-dimensional array of cartesian coordinates (cmt) read from a .xyz formatted file or .mol formatted file 
!> movemol: and writes the transformed geometry in a new .xyz file or .mol file.
!> movemol: The paremeters defining the transformations are read from the input file.
!> movemol: ind1, ind2 and ind3 are three 1-dimensional arrays containing a set of atom indices defining three different centroids.
!> movemol: The number of atoms indices defined in ind1, ind2 and ind3 (nat1, nat2 and nat3, respectively) must be previously given.
!> movemol: If ind1, ind2 or ind3 have only one element which is equal or inferior to zero, then the corresponding geometry transformation is disabled
!> movemol: The geometry is translated by a translation vector (cat0) and by taking the centroid1 (ind1) as reference.
!> movemol: The geometry is then rotated (centroid1 being the center of rotation) by aligning the axis defined by centroid1-centroid2 with a first alignement vector (align1)
!> movemol: The geometry is further rotated by aligning the axis defined by the altitude passing by centroid3 in (centroid1-centroid2-centroid3) with a second alignement vector (align2).
!> movemol: The point defined by the intersection between the altitude and the axis centroid1-centroid2 constitues the center of rotation.
!> movemol: The deletat option is used to delete some atoms used as markers (obsolete). If deletat=.true., then delete the atoms used as markers ; if deletat=.false., then do nothing.
!> movemol: A verbose option (verbose) controls the amount/type of information to be printed.

  program movemol

  implicit none

  integer i, j, k
  integer iuf
  integer fmode
  integer nat1, nat2, nat3, verbose
  double precision cat0(3), align1(3), align2(3)
  integer, dimension (:), allocatable :: ind1, ind2, ind3
  integer natm, nbnd
  character*2, dimension (:), allocatable :: atm
  double precision, dimension (:,:), allocatable :: cmt
  integer, dimension (:,:), allocatable :: bnd
  character*150 molfile, outputfile, getbasename, getextension
  logical deletat
  
    read(*,'(a)') molfile
    read(*,*) nat1
    allocate(ind1(nat1))
    read(*,*) (ind1(j), j=1,nat1)
    read(*,*) (cat0(j), j=1,3)
    read(*,*) nat2
    allocate(ind2(nat2))
    read(*,*) (ind2(j), j=1,nat2)
    read(*,*) (align1(j), j=1,3)
    read(*,*) nat3
    allocate(ind3(nat3))
    read(*,*) (ind3(j), j=1,nat3)
    read(*,*) (align2(j), j=1,3)
    read(*,*) deletat
    read(*,*) verbose

  !% Checks the extension of the .xyz file or .mol file
  call checkext(trim(molfile),2,(/"xyz","mol"/))
  if ( trim(getextension(molfile)) == "xyz" ) then
    fmode=0
  else
    fmode=1
  endif
  
  if ( fmode == 0 ) then
    outputfile=trim(getbasename(molfile))//"_a.xyz"
    !% Counts the number of atoms (natm)
    call numaxyzfile(trim(molfile),natm)
    allocate(cmt(natm,3),atm(natm))
    !% Reads and stores the 1-dimensional array of atom labels (atm) and the 2-dimensional array of cartesian coordinates (cmt)
    call readxyzfile(trim(molfile),atm,cmt,natm)
  else
    outputfile=trim(getbasename(molfile))//"_a.mol"
    !% Counts the number of atoms (natm) and bonds (nbnd)
    call  numamolfile(trim(molfile),natm,nbnd)
    allocate(cmt(natm,3),atm(natm))
    allocate(bnd(nbnd,3))
    call readmolfile(trim(molfile),cmt,atm,bnd,natm,nbnd)
  endif
  
  !% Translates and rotates the geometry by the translation vector cat0 and the alignement vectors align1 and align2
  call alignmol(natm,cmt,nat1,ind1,cat0,nat2,ind2,align1,nat3,ind3,align2,verbose,0)

  !% Delete the atoms used as markers
  if ( deletat ) then
    if ( verbose > 0 ) then
      write(*,*)
      write(*,*) "* Deleting referent atoms..."
    endif
    if ( nat1 == 1 .and. nat2 == 1 ) then
      k=0
      do i=1, natm
        if ( i .ne. ind1(1) .and. i .ne. ind2(1) ) then
          k=k+1
          atm(k)=atm(i)
          do j=1, 3
            cmt(k,j)=cmt(i,j)
          enddo
        endif
      enddo
      natm=k
    else
      write(*,*) "Warning!!! Cannot delete referent atoms when using multiple referent atoms..."
    endif
    write(*,*)
  endif
  
  if ( fmode == 0 ) then
    !% Write the transformed geometry in a new xyzfile
    call writexyzfile(trim(outputfile),atm,cmt,natm)
  else
    call writemolfile(trim(outputfile),atm,cmt,bnd,natm,nbnd)
    deallocate(bnd)
  endif

  deallocate(ind1,ind2,ind3)
  deallocate(cmt,atm)


  end program movemol

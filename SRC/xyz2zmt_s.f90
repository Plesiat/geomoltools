!> xyz2zmt_s: program xyz2zmt_s
!> xyz2zmt_s: Depends on getbasename, getextension, checkext, numaxyzfile, readxyzfile, uniqtab, convxyz2zmt, sortmat, reordna, zmtline, numafile, readtokfile, writezmtfile1, writezmtfile3
!> xyz2zmt_s: Converts the geometry defined by the cartesian coordinates contained in a .xyz formatted file (xyzfile) to a Z-matrix and writes it in a .zmt formatted file.
!> xyz2zmt_s: Sections (i.e., semi-independent block of atoms) in the Z-matrix can be define by setting a positive value to ncut or nsplit.
!> xyz2zmt_s: ncut and nsplit correspond to the number of atom indices in icut and isplit, each one defining where each section starts. 
!> xyz2zmt_s: The atoms following each of these atom indices will then be defined according only to the atoms in the section.
!> xyz2zmt_s: The value of nspecies determines if the Z-matrix will be stored in a standard format (nspecies=0) or in a format readable by Siesta (nspecies>0)
!> xyz2zmt_s: In the latter case, it is possible to give a set of atom indices for which we want the freezing/release options to be set to 1 (the default value being 1, i.e. frozen).
!> xyz2zmt_s: In the case of the atom indices icut, the freezing/release option will be setted up in such way that the "cutted" sections can move relatively to the previous section.
!> xyz2zmt_s: Optionally, it is possible to read a file (tokname) containing some bond, bond angles and torsions (specified in terms of atom indices) and  their respective force constants
!> xyz2zmt_s: and to set up automatically the freezing/release options by defining an upper limit to the force constants (kthd) for each one of the internal coordinates.
program xyz2zmt_s

  implicit none

  integer i, j, k, kount, kount2
  integer nsplit, ncut, nspecies
  integer natm, ntok, natm2, nspc, nmov(3)
  integer ixs, ixe, inda
  double precision kthd(3)
  integer, dimension (:), allocatable :: ispl, icut, imov, dmv, toktest
  integer, dimension (:), allocatable :: atmi
  integer, dimension (:,:), allocatable :: na, na2, amov
  character*2, dimension (:), allocatable :: atm, labat
  double precision, dimension (:), allocatable :: tok
  double precision, dimension (:,:), allocatable :: cmt, zmt
  character*20 kstr(3)
  character*150 xyzfile, tokname(3), outname, getbasename, getextension
  integer verbose

  verbose=0


  !% Checks the extension of xyzfile, counts the number of lines (natm) and reads/stores its values in the 1-dimensional array of atom labels (atm) and the 2-dimensional array of cartesian coordinates (cmt)
    read(*,'(a)') xyzfile
    call checkext(trim(xyzfile),1,"xyz")
    call numaxyzfile(trim(xyzfile),natm)
    allocate(cmt(natm,3),atm(natm))
    call readxyzfile(trim(xyzfile),atm,cmt,natm)
    read(*,*) ncut
    if ( ncut > 0 ) then
      allocate(icut(ncut))
      read(*,*) (icut(j), j=1, ncut)
    endif

    read(*,*) nsplit
    if ( nsplit > 0 ) then
      allocate(ispl(nsplit))
      read(*,*) (ispl(j), j=1, nsplit)
    endif
    
    read(*,*) nspecies
    if ( nspecies > 0 ) then
      !% If nspecies>0, reads the label of each atom contained in the molecule in the order required by Siesta
      allocate(labat(nspecies))
      read(*,*) (labat(j), j=1, nspecies)
      !% Reads and stores the number of atom indices to be released for each internal coordinate in nmov (bond, bond angle, torsions)
      read(*,*) (nmov(j), j=1, 3)
      if ( maxval(nmov) > natm ) then
        write(*,*)
        write(*,*)
        write(*,*) "Error!!! Maximal number of indices in nmov must be inferior to natm:"
        write(*,*) "maxval(nmov), natm=", maxval(nmov), natm
        write(*,*)
        stop
      endif
      !% Reads and stores the atom indices to be released for each internal cordinate in amov 
      allocate(amov(natm,3))
      amov=-1
      do j=1, 3
        if ( nmov(j) /= 0 ) then
          read(*,*) (amov(i,j), i=1, nmov(j))
          !% Delet the doublons in amov and redefine nmov accordingly
          call uniqtab(amov(:,j),nmov(j))
          if ( nmov(j) == 1 .and. amov(1,j) == 0 ) then
	    do i=1, natm
	      amov(i,j)=i
	    enddo
	  endif
        endif
      enddo

      !% Reads and stores the upper limit to the force constant for each internal coordinate in kthd
      read(*,*) (kthd(j), j=1, 3)
      do j=1, 3
	!% When the upper limiti to the force constant, reads the name of the file (tokname) which contains the specification of the corresponding internal coordinates and their force constants
        if ( kthd(j) /= 0.d0 ) then
          read(*,'(a)') tokname(j)
        endif
      enddo
    endif

  !% Stores the atom indices of icut and isplit in a unique 1-dimensional array (imov)
  nspc=ncut+nsplit
  if ( nspc > 0 ) then
    allocate(imov(nspc))
    do i=1, ncut
      imov(i)=icut(i)
    enddo
    do i=1, nsplit
      imov(i+ncut)=ispl(i)
    enddo
    call uniqtab(imov,nspc)
  endif


  allocate(na(natm,3),zmt(natm,3))

  if ( nspc == 0 ) then
    !% If no icut and isplit specified, converts the cartesian coordinates to Z-matrix in the current order of atoms
    na=0
    na(2,1)=1
    na(3,1)=2
    na(3,2)=1
    do i=4, natm
      do j=1, 3
        na(i,j)=na(i-1,j)+1
      enddo
    enddo
    call convxyz2zmt(cmt,natm,na,zmt,1)

  else
    
    !% If icut or isplit specified, proceed to the sectionning of the Z-matrix while converting it
    !% The list of atom indices is first sorted in the increasing order
    call sortmat(imov,nspc,1,1,1)

    !% The first section should start after the atom index 3 because relative motion requires the atom to be connected with 3 other atoms.
    if ( imov(1) <= 3 ) then
      write(*,*)
      write(*,*)
      write(*,*) "Error!!! Bad argument for option imov: imov(1)=", imov(1)
      write(*,*)
      stop
    endif

    allocate(dmv(nspc))
    do i=1, nspc-1
      dmv(i)=imov(i+1)-imov(i)
    enddo
    dmv(nspc)=natm-imov(nspc)+1
    if ( ANY(dmv<=3) ) then
      write(*,*)
      write(*,*)
      write(*,*) "Error!!! imov indeces must be spaced by at least 4: imov=", imov
      write(*,*)
      stop
    endif

    natm2=imov(1)-1
    allocate(na2(natm2,3))
    call reordna(cmt(1:natm2,:),natm2,na2)
    do j=1, natm2
      call zmtline(cmt,natm,j,na2(j,:),zmt(j,:))
      na(j,:)=na2(j,:)
    enddo
    deallocate(na2)
    
    !% Loop ruuning over each section
    do i=1, nspc

        ixs=imov(i)
        ixe=ixs+dmv(i)-1
        natm2=ixe-ixs+1
        allocate(na2(natm2,3))
        call reordna(cmt(ixs:ixe,:),natm2,na2)
        na2=na2+ixs-1
        do j=ixs, ixe
          call zmtline(cmt,natm,j,na2(j-ixs+1,:),zmt(j,:))
          na(j,:)=na2(j-ixs+1,:)
        enddo
        deallocate(na2)

        na(imov(i),:)=(/1, 2, 3/)
        na(imov(i)+1,:)=(/imov(i), 1, 2/)
        na(imov(i)+2,:)=(/imov(i)+1, imov(i), 1/)
        do j=imov(i), imov(i)+2
          call zmtline(cmt,natm,j,na(j,:),zmt(j,:))
        enddo

    enddo

    deallocate(dmv)

  endif


  if ( nspecies == 0 ) then
    
    !% Write the Z-matrix in a standard Z-matrix format
    outname=trim(getbasename(xyzfile))//".zms"
    call writezmtfile1("",atm,na,zmt,natm)

  else
  
    outname=trim(getbasename(xyzfile))//".zmt"
  
    if ( any(kthd/=0.d0) ) then
    
      kstr(1)="bond stretching"
      kstr(2)="angle bending"
      kstr(3)="torsions"
      !% Loop over the internal coordinate index
      do k=1, 3
	if ( kthd(k) /= 0.d0 ) then
	  
	  !% Counts the number of lines in tokname (ntok), reads and stores the internal coordinates specification (na2) and the respective force constants (tok)
	  !% Warning! The size of na2 varies with the type of internal coordinate: bond -> na2(ntok,2) ; bond angle -> na2(ntok,3) ; torsion -> na2(ntok,4)
	  call numafile(trim(tokname(k)),ntok)
	  allocate(na2(ntok,k+1),tok(ntok),toktest(ntok))
	  call readtokfile(trim(tokname(k)),na2,tok,ntok,k)
	  
	  toktest=0
	  do i=1, ntok
	    !% For each specified bond or bond angle or torsion specified, check if the force constant is below the threshold value kthd.
	    if ( tok(i) <= kthd(k) ) then
	      !% Loop which allows to read the atom indices specifying the internal coordinates in both directions: from na2(i,1) to na2(i,k+1) and from na2(i,k+1) to from na2(i,1)
	      do j=0, 1
		inda=na2(i,j*k+1)
		!% Checks if part of the internal coordinate is already defined in na (evaluated previously)
		if ( na(inda,1) == na2(i,j*(k-2)+2) ) then
		  !% For bonds and bond angles, the internal coordinates specified by na and na2 must match perfectly. For torsions, we allow the redifinition of the last atom index in na according to na2.
		  if ( k == 1 .or. k == 2 .and. na(inda,2) == na2(i,k-j) &
		  .or. k == 3 .and. na(inda,2) == na2(i,k-j) .and. na2(i,4-j*3) < inda ) then
		    if ( k == 3 ) then
		      na(inda,3)=na2(i,4-j*3)
		      !% Recalculate the dihedral angle according to the new value of na
		      call zmtline(cmt,natm,inda,na(inda,:),zmt(inda,:))
		    endif
		    toktest(i)=inda
		    !% Register the corresponding atom index to be release in amov, if not already registered
		    if ( ALL(amov(:,k)/=inda) ) then
		      nmov(k)=nmov(k)+1
		      amov(nmov(k),k)=inda
		    endif
		    exit
		  endif
		endif
	      enddo
	      if ( toktest(i) < 1 ) toktest(i)=-1
	    endif
	  enddo

	  !% Prints the list of atom indices which have been released.
	  if ( verbose == 1 ) then
	  write(*,*)
	  write(*,*) "-> ", trim(kstr(k)), " activation mode"
          if ( ANY(toktest>0) ) then
	    write(*,'(a,a,a)') "The following ",trim(kstr(k))," have been activated: "
	    do j=1, 3
	      do i=1, ntok
		if ( toktest(i) > 0 .and. ALL(toktest(1:i-1)/=toktest(i)) .and. j == 1) write(*,'(i5,$)'), i
		if ( toktest(i) > 0 .and. ALL(toktest(1:i-1)/=toktest(i)) .and. j == 2) write(*,'(i5,$)'), toktest(i)
		if ( toktest(i) > 0 .and. ALL(toktest(1:i-1)/=toktest(i)) .and. j == 3) write(*,'(f5.2,$)'), tok(i)
	      enddo
	      write(*,*)
	    enddo
	    write(*,*)
	    write(*,'(a,$)') "Repeated indexes: "
	    kount2=0
	    do i=1, natm
	      kount=COUNT(toktest==i)-1
	      if ( kount > 0 ) then
		write(*,'(i5,a,i2,a,$)'), i, "(",kount,")"
		kount2=kount2+kount
	      endif
	    enddo
	    write(*,*)
	  endif

	  write(*,*)
	  write(*,'(a,a,a,i6)') "Number of activated ",trim(kstr(k)),": ",COUNT(toktest>0)-kount2
	  write(*,*)

	  if ( ANY(toktest==-1) ) then
	    write(*,'(a,a,a)') "Some ",trim(kstr(k))," below threshold have not been taken into account: "
	    do j=1, 2
	      do i=1, ntok
		if ( toktest(i) == -1 .and. j == 1 ) write(*,'(i5,$)'), i
		if ( toktest(i) == -1 .and. j == 2 ) write(*,'(f5.2,$)'), tok(i)
	      enddo
	      write(*,*)
	    enddo
	  endif
          
          endif

	  deallocate(na2,tok,toktest)

	endif
      enddo
      
    endif
  
    
    !% Maps the list of specie indices with the list of atom labels
    allocate(atmi(natm))
    do i=1, natm
      do j=1, nspecies
        if ( atm(i) == labat(j) ) then
          atmi(i)=j
        endif
      enddo
    enddo

    !% If the atom index is contained in amov, set the corresponding freezing/release option to 1 in na2
    allocate(na2(natm,3))
    na2=0
    do i=1, natm
      do j=1, 3
        if ( ANY(amov(:,j)==i) .or. ANY(amov(:,j)==0) ) na2(i,j)=1
      enddo
    enddo
    na2(1,:)=0
    na2(2,2)=0
    na2(2,3)=0
    na2(3,3)=0

    do i=1, ncut
      inda=icut(i)
      na2(inda,:)=1
      na2(inda+1,2)=1
      na2(inda+1,3)=1
      na2(inda+2,3)=1
    enddo

    !% Writes the Z-matrix with the freezing/release options in a .zmt file
    call writezmtfile3("",atmi,na,na2,zmt,natm)
    deallocate(na2)
    deallocate(atmi)

  endif

  if ( ncut > 0 ) then
    deallocate(icut)
  endif
  if ( nsplit > 0 ) then
    deallocate(ispl)
  endif
  if ( nspc > 0 ) then
    deallocate(imov)
  endif
  if ( nspecies /= 0 ) then
    deallocate(labat)
      deallocate(amov)
  endif


  deallocate(cmt,atm,na,zmt)

  end

!> ucubcellgen: program ucubcellgen
!> ucubcellgen: getbasename, getextension, numaxyzfile, readxyzfile, xyzline, numafile, readzmtfile2, convzmt2xyz, writexyzfile
!> ucubcellgen: Calculates and prints the vectors of the unit cell according to the bond length, the bond angle and the dihedral angle (values given in zmt1d) 
!> ucubcellgen: between 3 atoms defined in inda and an atom ind0 in the following cell
!> ucubcellgen: The geometry of the molecule can be read in two formats: from a .xyz formatted file as cartesian coordinates or from a .zmt formatted file as a Z-matrix
  program ucubcellgen

  use vecop

  implicit none

  integer i, j, natm, ind0, ind1, ind2, inda(3), fmode, nspecies
  double precision zmt1d(3), cat1(3), ccell(3,3), dist1, dist2
  integer, dimension (:), allocatable :: atmi
  integer, dimension (:,:), allocatable :: na
  double precision, dimension (:,:), allocatable :: cmt, cmt2, zmt
  character*2, dimension (:), allocatable :: atm, atm2, labat
  character*150 inpname, outname, getbasename, getextension

    read(*,'(a)') inpname
    !% Depending on the format of the file we defines 2 modes: fmode=1 and fmode=2
    select case (trim(getextension(inpname)))
    case("xyz")
      fmode=1
    case("zmt")
      fmode=2
    case default
      write(*,*)
      write(*,*)
      write(*,*) "Error!!! Bad extension for inputfile!"
      write(*,*)
      stop
    end select
    
    read(*,*) ind0, (inda(j), j=1, 3), (zmt1d(j), j=1, 3)
    read(*,*) ind1, ind2, dist1
    read(*,*) dist2
    if ( fmode == 2 ) then
      read(*,*) nspecies
      if ( nspecies /= 0 ) then
        allocate(labat(nspecies))
        read(*,*) (labat(j), j=1, nspecies)
      endif
    endif

  outname=trim(getbasename(inpname))//"_supc.xyz"

  if ( fmode == 1 ) then
  
    !% Reads the number of atoms (natm)
    call numaxyzfile(trim(inpname),natm)
    allocate(atm(natm),cmt(natm,3))
    call readxyzfile(trim(inpname),atm,cmt,natm)

  else
    
    !% Counts the number of atoms (natm) and read the Z-matrix (defined by na and zmt)
    call numafile(trim(inpname),natm)
    allocate(atmi(natm),na(natm,3),zmt(natm,3))
    call readzmtfile2(trim(inpname),atmi,na,zmt,natm)
    allocate(atm(natm),cmt(natm,3))
    
    !% Convert the Z-matrix to cartesian coordinates contained in the 2-dimensional array (cmt)
    call convzmt2xyz(zmt,cmt,na,natm)
    cmt(:,1)=-cmt(:,1)
    cmt(:,2)=-cmt(:,2)

    if ( nspecies /= 0 ) then
      do i=1, natm
        if ( atmi(i) < 1 .or. atmi(i) > nspecies ) then
          write(*,*)
          write(*,*) "Error!!! Specie index from Zmatrix out of range!"
          write(*,*) "i, atmi(i)=", i, atmi(i)
          write(*,*)
          stop
        endif
        atm(i)=labat(atmi(i))
      enddo
    endif

    deallocate(atmi,labat)

  endif

  !% Calculate the cartesian coordinates (cat) of the connected atom in the next cell
  call xyzline(cmt,zmt1d,inda,natm,cat1)

  ccell=0.d0
  
  !% We first calculates vx vector of the new unit cell (ccell(1,:)) which is simply defined by the difference between the spatial coordinates of ind0 in two adjacent cells
  ccell(1,:)=cat1-cmt(ind0,:)
  
  !% We treats two different cases: when the axis y of the new unit cell can simply be defined by one atom (ind1 = ind2) or when it is required to specify 2 atoms (ind1 /= ind2)
  if ( ind1 == ind2 ) then
    cat1=cat1-cmt(ind1,:)
  else
    cat1=cmt(ind2,:)-cmt(ind1,:)
  endif
  cat1=cat1/normvec(cat1)
  
  !% The vector vz is calculated first because it can be defined immediately because it is perpendicular to the plane defined by the vector vx and the axis ind1-ind2 (containing the new vy).
  ccell(3,:)=crossprod(ccell(1,:),cat1)
  ccell(3,:)=dist2*ccell(3,:)/normvec(ccell(3,:))
  
  !% The vector vy can then be calculated from the vector vz and vx
  ccell(2,:)=crossprod(ccell(3,:),ccell(1,:))
  ccell(2,:)=ccell(2,:)/normvec(ccell(2,:))
  
  if ( ind1 /= ind2 ) dist1=dot_product(dist1*cat1+cmt(ind2,:)-cmt(ind1,:),ccell(2,:))
  ccell(2,:)=dist1*ccell(2,:)


  do i=1, 3
    write(*,'(3f12.6)') (ccell(i,j), j=1,3)
  enddo

  if ( nspecies /= 0 ) then
    allocate(cmt2(4*natm,3),atm2(4*natm))
    do i=1, natm
      atm2(i)=atm(i)
      !% We project the cartesian coordinates in the basis of orthonormal vectors of the new cubic unit cell
      do j=1, 3
	cmt2(i,j)=dot_product(cmt(i,:),ccell(j,:)/normvec(ccell(j,:)))
      enddo
    enddo
    do j=1, 3
      do i=j*natm+1, (j+1)*natm
        atm2(i)=atm(i-j*natm)
	cmt2(i,:)=cmt2(i-j*natm,:)
	!% Each value of j represents a direction, and for each of these components we add the value of cell in the cooresponding direction
	cmt2(i,j)=cmt2(i,j)+normvec(ccell(j,:))
      enddo
    enddo

    call writexyzfile(trim(outname),atm2,cmt2,4*natm)

    deallocate(cmt2,atm2)
  endif

  deallocate(cmt,atm)

  end

!> zmt2xyz_s: program zmt2xyz_s
!> zmt2xyz_s: Depends on checkext, getextension, getbasename, numafile, readzmtfile2, convzmt2xyz, writexyzfile
!> zmt2xyz_s: Converts a Z-matrix defined by the 1-dimensional array of atom indices na and the 2-dimensional array containing the values of the corresponding internal coordinates (zmt) 
!> zmt2xyz_s: read from a .zms or .zmt formatted file into a 2-dimensional array of cartesian coordinates (cmt) which is written in a .xyz formatted file.
!> zmt2xyz_s: If nspecies=0, the .zmt file is assumed to be written in a standard format where atom labels are given by characters.
!> zmt2xyz_s: If nspecies>0, the .zmt file is assumed to be written in a format readable by Siesta 
!> zmt2xyz_s: and the 1-dimensional array of atom labels is generated according to the order of the atom labels given in the inputfile

  program zmt2xyz_s

  integer i, j, natm, nspecies
  integer zmode
  integer, dimension (:), allocatable :: atm2
  integer, dimension (:,:), allocatable :: na
  double precision, dimension (:,:), allocatable :: cmt, zmt
  character*2, dimension (:), allocatable :: labat, atm
  character*150 zmtfile, outname
  character*150 getextension, getbasename
  
  integer iuf

    read(*,'(a)') zmtfile
    
    call checkext(trim(zmtfile),2,(/"zms","zmt"/))
    if ( trim(getextension(trim(zmtfile))) == "zms" ) then
      zmode=0
    else
      zmode=1
    endif

    if ( zmode == 1 ) then
      read(*,*) nspecies
      if ( nspecies > 0 ) then
	allocate(labat(nspecies))
	read(*,*) (labat(i), i=1, nspecies)
      endif
    endif

    outname=trim(getbasename(trim(zmtfile)))//".xyz"
  
  if ( zmode == 0 ) then
    !% Reads the number of atoms
    call numaxyzfile(trim(zmtfile),natm)
    allocate(atm(natm),na(natm,3),zmt(natm,3))
    !% Reads and stores atm, na and zmt from a standard .zms file
    call readzmtfile1(trim(zmtfile),atm,na,zmt,natm)
  else
    !% Counts the number of atoms
    call numafile(trim(zmtfile),natm)
    allocate(atm2(natm))
    allocate(atm(natm),na(natm,3),zmt(natm,3))
    !% Reads and stores atm2, na and zmt from a .zmt file readable by Siesta
    call readzmtfile2(trim(zmtfile),atm2,na,zmt,natm)
  endif
  
  allocate(cmt(natm,3))
  !% Converts the Z-matrix (defined by na and zmt) into a 2-dimensional array of cartesian coordinates (cmt)
  call convzmt2xyz(zmt,cmt,na,natm)
  !% This is made in order to stick to the Siesta format
  cmt(:,1)=-cmt(:,1)
  cmt(:,2)=-cmt(:,2)

  !% If we have the atom labels as specie index, generates and checks the 1-dimensional array of atom labels
  if ( zmode == 1 ) then
    if ( nspecies > 0 ) then
      do i=1, natm
	if ( atm2(i) < 1 .or. atm2(i) > nspecies ) then
	  write(*,*)
	  write(*,*) "Error!!! Specie index from Zmatrix out of range: atm2(i)=", atm2(i)
	  write(*,*)
	  stop
	endif
	atm(i)=labat(atm2(i))
      enddo
    endif
  endif

  !% Writes atm and cmt in a xyzfile
  if ( zmode == 0 .or. zmode == 1 .and. nspecies > 0 ) then
    call writexyzfile("",atm,cmt,natm)
  else
    call writexyzfile2("",atm2,cmt,natm)
  endif

  deallocate(atm,na,zmt,cmt)
  if ( zmode == 1 ) then
    deallocate(atm2)
    if ( nspecies /= 0 ) deallocate(labat)
  endif

  end

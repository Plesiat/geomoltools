
!> joinmol: subroutine joinmol(gtest,cmt1,atm1,natm1,cmt2,atm2,natm2,verbose,iuf2,cmt3,atm3)
!> joinmol: Depends on minmaxmat
!> joinmol: Joins two geometries defined by the 2-dimensional arrays of cartesian coordinates (cmt1 and cmt2) and stores the gathered geometry in a new 2-dimensional arrays of cartesian coordinates (mat3)
!> joinmol: depending on a boolean value (gtest)
subroutine joinmol(gtest,cmt1,atm1,natm1,cmt2,atm2,natm2,verbose,iuf2,cmt3,atm3)

  implicit none

  logical, intent(in) :: gtest
  integer, intent(in) :: natm1, natm2
  double precision, intent(in) :: cmt1(natm1,3), cmt2(natm2,3)
  character*(*), intent(in) :: atm1(natm1), atm2(natm2)
  integer, intent(in) :: verbose, iuf2(4)
  double precision, intent(out) :: cmt3(natm1+natm2,3)
  character*(*), intent(out) :: atm3(natm1+natm2)
  
  integer i, ind1, ind2
  double precision distrf

  !% Join the two geometries in one if the criterium is fulfilled. Filled atm3 and cmt3 with dummy values otherwise.
  if ( gtest ) then
    do i=1, natm1
      cmt3(i,:)=cmt1(i,:)
      atm3(i)=atm1(i)
    enddo
    do i=natm1+1, natm1+natm2
      cmt3(i,:)=cmt2(i-natm1,:)
      atm3(i)=atm2(i-natm1)
    enddo
    if ( verbose == 2 ) then
      call minmaxmat(cmt1,cmt2,natm1,natm2,-1,ind1,ind2,distrf)
      write(iuf2(1),*) cmt2(ind2,:)
      write(iuf2(2),*) distrf
      call minmaxmat(cmt1,cmt2,natm1,natm2,1,ind1,ind2,distrf)
      write(iuf2(3),*) cmt2(ind2,:)
      write(iuf2(4),*) distrf
    endif
  else
    cmt3=0.d0
    atm3="X"
  endif
  
end subroutine joinmol


!> stacktest: subroutine stacktest(cmt1,natm1,cmt2a,natm2,modtrs,valtrs,verbose,gtest)
!> stacktest: Depends on getmodind, minmaxmat, compctrs, checkminmax
!> stacktest: Test the stacking of two geometries defined by the 2-dimensional arrays of cartesian coordinates (cmt1 and cmt2) 
!> stacktest: according to some criteria defined by a label (modtrs) and 2 numerical values (valtrs) and return a boolean value (gtest)
!> stacktest: modtrs can takes the following values: "rminr","rminx","rminy","rminz","rmaxr","rmaxx","rmaxy","rmaxz","rlim". Any other values of modtrs will disable the application of a criterium.
!> stacktest: valtrs(1) and valtrs(2) defines two distances between an atom from geometry1 and an atom from geometry2 to be used as limits.
!> stacktest: modtrs="rminr","rminx","rminy","rminz" leads to an iterative displacement of geometry2 in the direction r,x,y and z respectively 
!> stacktest: until the minimum distance between two nearest atoms belonging to the different geometries is equal to valtrs(1).
!> stacktest: modtrs="rmaxr","rmaxx","rmaxy","rmaxz" leads to an iterative displacement of geometry2 in the direction r,x,y and z respectively 
!> stacktest: until the maximum distance between two nearest atoms belonging to the different geometries is equal to valtrs(1).
!> stacktest: modtrs="rlim" permits to checks simply if the minimum (maximum) distance between two nearest atoms belonging to the different geometries is higher (lower) than valtrs(1)(valtrs(2)).
!> stacktest: In any cases, a logical value (gtest) is assigned .true. if the required criterium is fulfilled and .false. if not.
!> stacktest: A verbose option (verbose) controls the amount/type of information to be printed.
!> stacktest: The 1-dimensional arrays containing the atom labels for both geometries (atm1 and atm2) and the size of cmt1 (natm1) and cmt2 (natm2) must be given.
  subroutine stacktest(cmt1,natm1,cmt2a,natm2,modtrs,valtrs,verbose,gtest)

  implicit none

  integer, intent(in) :: natm1, natm2
  double precision, intent(in) :: cmt1(natm1,3)
  character*(*), intent(in) :: modtrs
  double precision, intent(in) :: valtrs(2)
  integer, intent(in) :: verbose
  logical, intent(out) :: gtest
  double precision, intent(inout) :: cmt2a(natm2,3)

  integer i, j, k, kount
  integer ind1, ind2, indm, indk
  double precision cmt2b(natm2,3)
  double precision distrf, ctrs(3)
  integer glim
  parameter (glim=20)

  gtest=.true.
  
  !% Assign a standard value to indm and indk depending on the value of modtrs
  call getmodind(modtrs,indm,indk)

  !% Select the kind of criterium to be used. If indm /= 0,-1,1, no criterium is used (gtest=.true.) and no modifications are applied to geometry2.
  select case (indm)
  case(0)
    !% Checks that geometry1 and geometry2 lay in the range of interatomic distance defined by valtrs(1) and valtrs(2) and returns gtest=.true. if the criterium is respected and gtest=.false if not.
    call checkminmax(cmt1,cmt2a,natm1,natm2,indm,valtrs,gtest)
  case(-1,1)
    cmt2b=cmt2a
    gtest=.false.
    kount=0
    do while ( .not. gtest .and. kount <= glim )
      !% Computes the minimal (or maximal) distance (distrf) between two nearest atoms belonging to the different geometries and retrieves their respective atom indices (ind1 and ind2)
      call minmaxmat(cmt1,cmt2b,natm1,natm2,indm,ind1,ind2,distrf)
      !% Compute the translation vector required to bring the minimal (or maximal) distance to valtrs(1) by translating geometry2 in a specific direction (r, x, y, or z)
      call compctrs(cmt1(ind1,:),cmt2b(ind2,:),valtrs(1),ctrs,indk)
      do i=1, natm2
	cmt2b(i,:)=cmt2b(i,:)+ctrs
      enddo
      
      !% Checks if the criterium is fulfilled. If not, redo the process until the criterium is fulfilled or the number of iterations exceeds the limit (glim)
      call checkminmax(cmt1,cmt2b,natm1,natm2,indm,valtrs,gtest)
      kount=kount+1
    enddo
    
    if ( verbose > 0 ) then
      if ( gtest ) then
	write(*,'(a)') "rmin/rmax searching converged!"
	write(*,'(a,i3)') "-> Number of iterations: ", kount
	write(*,'(a,2i4)') "-> Related atoms: ", ind1, natm1+ind2
      else
	write(*,'(a)') "rmin/rmax searching did not converged!"
      endif
    endif
    
    !% If indm=-1 or 1 and gtest=.true., we take the transformed geometry
    if ( gtest ) cmt2a=cmt2b
  end select

end subroutine stacktest



!> getmodind: subroutine getmodind(modtrs,indm,indk)
!> getmodind: Returns standard values (indm and indk) according to the choosen criterium (modtrs)
!> getmodind: min->indm=-1 ; max->indm=1 ; default->indm=99 ; x->indk=1 ; y->indk=2 ; z->indk=3 ; r->indk=0 ; default->indk=99
  subroutine getmodind(modtrs,indm,indk)

    implicit none

    character*(*), intent(in) :: modtrs
    integer, intent(out) :: indm, indk

    select case (modtrs)
    case("rminr","rminx","rminy","rminz")
      indm=-1
    case("rmaxr","rmaxx","rmaxy","rmaxz")
      indm=1
    case("rlim")
      indm=0
    case default
      indm=99
    end select

    select case (modtrs)
    case("rminr","rmaxr")
      indk=0
    case("rminx","rmaxx")
      indk=1
    case("rminy","rmaxy")
      indk=2
    case("rminz","rmaxz")
      indk=3
    case default
      indk=99
    end select

    return

  end subroutine getmodind

!> minmaxmat: subroutine minmaxmat(cmt1,cmt2,natm1,natm2,indm,ind1,ind2,distrf)
!> minmaxmat: Depends on normvec.
!> minmaxmat: Computes the minimal (when indm=-1) or maximal (when indm=1) distance (distrf) between two nearest atoms belonging to two different geometries 
!> minmaxmat: defined by the 2-dimensional arrays of cartesian coordinates (cmt1 and cmt2) and retrieves the corresponding atom indices (ind1 and ind2).
!> minmaxmat: The size of cmt1 (natm1) and cmt2 (natm2) must be given.
  subroutine minmaxmat(cmt1,cmt2,natm1,natm2,indm,ind1,ind2,distrf)
  
    use vecop

    implicit none

    integer, intent(in) :: natm1, natm2, indm
    double precision, intent(in) :: cmt1(natm1,3), cmt2(natm2,3)
    integer, intent(out) :: ind1, ind2
    double precision, intent(out) :: distrf

    integer i, j, k
    integer ind1b, ind2b
    double precision distat, distrfb
    
    ind1=0
    ind2=0
    ind1b=0
    ind2b=0
    select case (indm)
    case(-1)
      distrf=10000.d0
       do i=1, natm1
	do j=1, natm2
	  distat=normvec(cmt1(i,:)-cmt2(j,:))
	  if ( distat < distrf ) then
	    distrf=distat
	    ind1=i
	    ind2=j
	  endif
	enddo
      enddo
    case(1)
      distrfb=0.d0
      do j=1, natm2
	distrf=10000.d0
	do i=1, natm1
	  distat=normvec(cmt1(i,:)-cmt2(j,:))
	  if ( distat < distrf ) then
	    distrf=distat
	    ind1=i
	    ind2=j
	  endif
	enddo
	if ( distrf > distrfb ) then
	  distrfb=distrf
	  ind1b=ind1
	  ind2b=ind2
	endif
      enddo
      distrf=distrfb
      ind1=ind1b
      ind2=ind2b
    case default
      write(*,*)
      write(*,*) "Error!!! Bad argument for option indm: indm=", indm
      write(*,*)
      stop
    end select

    if ( ind1 == 0 .or. ind2 == 0 ) then
      write(*,*)
      write(*,*) "Error!!! Min/max distance equals to zero."
      write(*,*)
      stop
    endif

    return

  end subroutine minmaxmat

!> compctrs: subroutine compctrs(cat1,cat2,distf,ctrs,indk)
!> compctrs: Depends on normvec.
!> compctrs: Computes the translation vector (ctrs) required to make the cartesian coordinates (cat1 and cat2) distant by distf when translating in a specific direction (r, x, y, or z) defined by indk.
!> compctrs: indk can takes the following values: 0, 1, 2, 3 which correspond to the direction r, x, y, z respectively.
  subroutine compctrs(cat1,cat2,distf,ctrs,indk)
  
    use vecop

    implicit none

    double precision, intent(in) :: cat1(3), cat2(3)
    double precision, intent(in) :: distf
    integer, intent(in) :: indk
    double precision, intent(out) :: ctrs(3)

    integer i
    double precision disti, fact
    
    disti=normvec(cat2-cat1)

    if ( indk == 0 ) then
      if ( disti == 0.d0 ) then
	ctrs=distf/dsqrt(3.d0)
      else
	fact=(distf/disti-1.d0)
        ctrs=(cat2-cat1)*fact
      endif
    else
      ctrs=0.d0
      ctrs(indk)=distf**2
      do i=1, 3
	if ( i /= indk ) ctrs(indk)=ctrs(indk)-(cat2(i)-cat1(i))**2
      enddo
      ctrs(indk)=dsqrt(ctrs(indk))-(cat2(indk)-cat1(indk))
    endif

    return

  end subroutine compctrs


!> checkminmax: subroutine checkminmax(cmt1,cmt2,natm1,natm2,indm,valtrs1,gtest)
!> checkminmax: Depends on minmaxmat.
!> checkminmax: Checks if the minimal/minimal/minimal+maximal distance between two nearest atoms belonging to two different geometries defined by the 2-dimensional arrays of cartesian coordinates (cmt1 and cmt2)
!> checkminmax: is higher/lower/higher+lower than the numerical value(s) valtrs(1)/valtrs(1)/valtrs(1)+valtrs(2) depending if indm is equal to -1/1/0
!> checkminmax: and returns gtest=.true. if the criterium is fulfilled and gtest=.false. if not.
!> checkminmax: If indm=0 and valtrs(1)=valtrs(2), then gtest=.true. only if the minimum distance between two nearest atoms belonging to two different geometries matches valtrs(1).
!> checkminmax: Tolerances for the numerical comparison of the distances are assigned internally.
!> checkminmax: The size of cmt1 (natm1) and cmt2 (natm2) must be given.
  subroutine checkminmax(cmt1,cmt2,natm1,natm2,indm,valtrs,gtest)

    implicit none

    integer, intent(in) :: natm1, natm2, indm
    double precision, intent(in) :: cmt1(natm1,3), cmt2(natm2,3)
    double precision, intent(in) :: valtrs(2)
    logical, intent(out) :: gtest

    integer i, k
    integer ind1, ind2
    double precision tol1, tol2, distrf
    parameter (tol1=1.d2,tol2=1.d4)

    select case (indm)
    
    case(0)
    if ( valtrs(1) > valtrs(2) ) then
      write(*,*)
      write(*,*) "Error!!! Low limit valtrs(1) should be inferior or equal to valtrs(2)!"
      write(*,*) "valtrs(1), valtrs(2)= ", valtrs(1), valtrs(2)
      write(*,*)
    endif
    if ( valtrs(1) == valtrs(2) ) then
      !% Only geometries2 with minimum distance to nearest atom of geometry1 equals to valtrs(1) will be accepted (warning: tolerance has here a lower value).
      call minmaxmat(cmt1,cmt2,natm1,natm2,-1,ind1,ind2,distrf)
      if ( nint(valtrs(1)*tol1)-nint(distrf*tol1) /= 0 ) then
	gtest=.false.
      else
	gtest=.true.
      endif
    else
     !% Only geometries2 with minimum and maximum distances to nearest atom of geometry1 lying in the range valtrs(1):valtrs(2) will be accepted.
      k=1
      do i=-1, 1, 2
	call minmaxmat(cmt1,cmt2,natm1,natm2,i,ind1,ind2,distrf)
	if ( dble(i)*(nint(valtrs(k)*tol2)-nint(distrf*tol2)) < 0 ) then
	  gtest=.false.
	  exit
	else
	  gtest=.true.
	endif
	k=k+1
      enddo
    endif
    
    case(-1,1)
    call minmaxmat(cmt1,cmt2,natm1,natm2,indm,ind1,ind2,distrf)
    if ( nint(valtrs(1)*tol2)-nint(distrf*tol2) /= 0 ) then
      gtest=.false.
    else
      gtest=.true.
    endif
    
    end select

  end subroutine checkminmax

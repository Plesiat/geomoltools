
!> vecop: module vecop
!> vecop: This module contains functions and subroutines for basic vector operations: 
!> vecop: norm, cross product, orthogonal vector generator,
!> vecop: conversion of polar and azimuthal angles to normalized vector in cartesian coordinates
module vecop

implicit none

contains

!> normvec: double precision function normvec(u1)
!> normvec: Computes the norm of the vector u1(u1x,u1y,u1z)
double precision function normvec(u1)

  implicit none

  double precision, intent(in) :: u1(3)

  normvec=dsqrt(u1(1)**2+u1(2)**2+u1(3)**2)

  return

end function normvec

!> crossprod: function crossprod(u1,u2) - double precision
!> crossprod: Computes the cross product of the vectors u1(u1x,u1y,u1z) and u2(u2x,u2y,u2z)
function crossprod(u1,u2)

  implicit none

  double precision, intent(in) :: u1(3), u2(3)
  double precision crossprod(3)

  crossprod(1)=u1(2)*u2(3)-u1(3)*u2(2)
  crossprod(2)=u1(3)*u2(1)-u1(1)*u2(3)
  crossprod(3)=u1(1)*u2(2)-u1(2)*u2(1)

end function crossprod

!> genorthvec: function genorthvec(u1) - double precision
!> genorthvec: Depends on crossprod
!> genorthvec: Generates automatically an arbitrary orthogonal vector to u1(u1x,u1y,u1z)
function genorthvec(u1)

  implicit none

  double precision, intent(in) :: u1(3)
  double precision genorthvec(3)

  double precision u0(3), u2(3)

  u2=0.d0
  !% Let's take an arbitrary vector u2 to generate 
  !% an arbitrary orthognal vector u0 to the plane defined by u1, u2
  u2(1)=1.d0
  u0=crossprod(u1,u2)
  !% If by any chance u1 and u2 are collinear, let's take another arbitrary vector u2
  if ( normvec(u0) == 0.d0 ) then
    u2=0.d0
    u2(2)=1.d0
    u0=crossprod(u1,u2)
  endif
  genorthvec=u0

end function genorthvec

!> ang2vec: function ang2vec(theta,phi,amode) - double precision
!> ang2vec: Converts the polar (theta) and azimuthal (phi) angles 
!> ang2vec: to a normalized vector in cartesian coordinates.
!> ang2vec: amode (= "degree" or "radian") defines the unit of the given angles
function ang2vec(theta,phi,amode)

  implicit none

  double precision, intent(in) :: theta, phi
  character*(*), intent(in) :: amode
  double precision ang2vec(3)

  double precision theta2, phi2
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    theta2=theta*pig/180.d0
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      theta2=theta
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif

  ang2vec(1)=dsin(theta2)*dcos(phi2)
  ang2vec(2)=dsin(theta2)*dsin(phi2)
  ang2vec(3)=dcos(theta2)

end function ang2vec


!> vecrotperplan: function vecrotperplan(u0,u1,phi,amode) - double precision
!> vecrotperplan: Depends on crossprod, normvec
!> vecrotperplan: Calculates a vector that belongs to the plan perpendicular u0 and containing u1, and which is rotated by an angle phi (clockwise) with respect to u1
function vecrotperplan(u0,u1,phi,amode)
  
  implicit none
  
  double precision, intent(in) :: u0(3), u1(3)
  double precision, intent(in) :: phi
  character*(*), intent(in) :: amode
  double precision vecrotperplan(3)
  
  double precision phi2
  double precision u1b(3)
  double precision u2(3)
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif
  
  !% Calculate the second basis vector (perpendicular to u0)
  u2=crossprod(u0,u1)
  u2=u2/normvec(u2)
  u1b=u1/normvec(u1)
  
  !% Calculate the required vector in the normalized vector bases
  vecrotperplan=dcos(phi2)*u1b+dsin(phi2)*u2
  
  end function vecrotperplan
  
  
!> vecrotperplan2: function vecrotperplan(u0,u1,phi,amode) - double precision
!> vecrotperplan2: Depends on crossprod, normvec
!> vecrotperplan2: 
function vecrotperplan2(u0,u1,phi,amode)
  
  implicit none
  
  double precision, intent(in) :: u0(3), u1(3)
  double precision, intent(in) :: phi
  character*(*), intent(in) :: amode
  double precision vecrotperplan2(3)
  
  double precision phi2
  double precision u1b(3)
  double precision u2(3)
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif
  
  !% Calculate the first basis vector (perpendicular to u0 and u1)
  u1b=crossprod(u0,u1)
  !% Calculate the second basis vector (perpendicular to u0 and u1b)
  u2=crossprod(u0,u1b)
  u1b=u1b/normvec(u1b)
  u2=u2/normvec(u2)
  
  !% Calculate the required vector in the normalized vector bases
  vecrotperplan2=dcos(phi2)*u1b+dsin(phi2)*u2
  
  end function vecrotperplan2
  

end module vecop


!> numdelstr: integer function numdelstr(strg,delim)
!> numdelstr: Counts the number of specific delimiter delim in the string strg
  integer function numdelstr(strg,delim)

    implicit none

    character*(*), intent(in) :: strg
    character*(*), intent(in) :: delim

    integer i

    i=1
    numdelstr=count((/(strg(i:i),i=1,len_trim(strg))/) == trim(delim))

  end function numdelstr

!> numfldstr: integer function numfldstr(strg,delim)
!> numfldstr: Depends on numdelstr
!> numfldstr: Counts the number of fields in the string strg according to the delimiter delim
  integer function numfldstr(strg,delim)

    implicit none

    character*(*), intent(in) :: strg
    character*(*), intent(in) :: delim

    integer lasti
    integer numdelstr

    lasti=len_trim(strg)

    numfldstr=numdelstr(trim(strg),trim(delim))
    if ( strg(1:1) == trim(delim) ) then
      if ( strg(lasti:) == trim(delim) ) then
	numfldstr=numfldstr-1
      endif
    else
      if ( strg(lasti:) /= trim(delim) ) then
	numfldstr=numfldstr+1
      endif
    endif

  end function numfldstr

!> splitstr: subroutine splitstr(strg,delim,str1,str2)
!> splitstr: Splits the string strg in two strings (str1 and str2)
!> splitstr: according to the first delimiter delim found
  subroutine splitstr(strg,delim,str1,str2)

    character*(*), intent(in) :: strg
    character*(*), intent(in) :: delim
    character*(*), intent(out) :: str1, str2

    integer indel

    indel=scan(trim(strg),delim)
    str1=strg(1:indel-1)
    str2=strg(indel+1:)

  end subroutine splitstr

!> sequify: subroutine sequify(strind,nind1,ind,nind2)
!> sequify: Depends on splitstr, numdelstr
!> sequify: Generates a sequence of integers from the string strind.
!> sequify: strind must contained integers separated by the delimiters "," and ":"
!> sequify: "," = and ; ":"=range of values (e.g., 3:5=3,4,5)
!> sequify: nind1=size of the array of integers
!> sequify: nind2=number of integers in the sequence
  subroutine sequify(strind,nind1,ind,nind2)

    implicit none

    character*(*), intent(in) :: strind
    integer, intent(in) :: nind1
    integer, intent(out) :: ind(nind1)
    integer, intent(out) :: nind2

    integer i, k, ndel
    character*150 str1, str2, str3, str4
    integer imin, imax, inc
    integer numdelstr

    str2=trim(strind)
    k=0
    !% The loop goes through each field in strind delimited by ","
    do while ( trim(str2) /= "" )
      call splitstr(trim(str2),",",str1,str2)
      if ( trim(str1) == "" ) then
	str1=str2
	str2=""
      endif
      !% Count the number of delimiters ":" in the extracted field
      ndel=numdelstr(trim(str1),":")
      if ( ndel > 1 ) then
	write(*,*)
	write(*,*) "Error!!! Two many delimiter - in the field: ndel=", ndel
	write(*,*)
	stop
      endif
      
      if ( ndel == 1 ) then
	!% If there is one delimiter ":" in the extracted field, generate the corresponding sequence of integers
	call splitstr(trim(str1),":",str3,str4)
	read(str3,*) imin
	read(str4,*) imax
	if ( imin < imax ) then
	  !% Increasing order
	  inc=1
	else
	  !% Decreasing order
	  inc=-1
	endif
      else
	!% If there is no delimiter ":" in the extracted field, set up imin=imax, 
	!% so the next loop will run on one index only (corresponding to the extracted field)
	read(str1,*) imin
	imax=imin
	inc=1
      endif
      
      do i=imin, imax, inc
	k=k+1
	if ( k > nind1 ) then
	  write(*,*)
	  write(*,*) "Error!!! Field number is higher than given array dimension: k, nind=", k, nind1
	  write(*,*)
	  stop
	endif
	ind(k)=i
      enddo
    enddo

    nind2=k

  end subroutine sequify


!> nb2car_zpad: subroutine nb2car_zpad(nb,nz,car)
!> nb2car_zpad: Converts an integer (nb) in character (car) and pads nz zeros as a prefix
  subroutine nb2car_zpad(nb,nz,car)

    implicit none

    integer, intent(in) :: nb, nz
    character*(*), intent(out) :: car

    character*15 FMT1

    write(FMT1,'(a,i6,a)') "(i0.",nz,")"
    write(car,FMT1) nb

  end subroutine nb2car_zpad
  
!> getbasename: character*150 function getbasename(name)
!> getbasename: Extracts the basename of the filename name (i.e., without the extension part)
character*150 function getbasename(name)

  implicit none

  character*(*), intent(in) :: name

  integer ppos

  ppos=0
  ppos=scan(trim(name),".", BACK= .true.)
  if ( ppos > 0 ) then
    getbasename=name(1:ppos-1)
  else
    getbasename=name
  endif

  return

end function getbasename

!> getextension: character*150 function getextension(name)
!> getextension: Extracts the extension of the filename name (i.e., without the basename part)
character*150 function getextension(name)

  implicit none

  character*(*), intent(in) :: name

  integer ppos

  ppos=0
  ppos=scan(trim(name),".", BACK= .true.)
  if ( ppos > 0 ) then
    getextension=name(ppos+1:)
  else
    getextension=name
  endif

  return

end function getextension

!> checkext: subroutine checkext(name, next, extarray)
!> checkext: Depends on getextension
!> checkext: Checks that the extension of the filename name corresponds to one of the authorized extensions (extarray)
!> checkext: next=size of the array extarray ; extarray must contain all the authorized extension for filename name
subroutine checkext(name, next, extarray)

  implicit none

  character*(*), intent(in) :: name
  integer, intent(in) :: next
  character*(*), intent(in) :: extarray(next)

  integer i
  character*150 getextension
  character*10 ext
  logical etest

  ext=trim(getextension(name))
  etest=.true.
  do i=1, next
    if ( trim(ext) == trim(extarray(i)) ) then
      etest=.false.
    endif
  enddo
  if ( etest ) then
    write(*,*)
    write(*,*) "Error!!! Bad extension for file: ", trim(name)
    write(*,*)
    stop
  endif

  return

end subroutine checkext

!> checkfile: subroutine checkfile(filename)
!> checkfile: Checks that the file with filename filename exists
subroutine checkfile(filename)

  implicit none

  character*(*), intent(in) :: filename

  logical ftest

  ftest=.false.
	inquire(FILE=trim(filename), EXIST=ftest)
	if ( .not. ftest) then
    write(*,*)
    write(*,*) "Error!!! Inputfile not found: ", trim(filename)
    write(*,*)
    stop
  endif

end subroutine checkfile

!> numafile: subroutine numafile(filename,nline)
!> numafile: Depends on checkfile
!> numafile: Counts the number of lines of the file with filename filename
subroutine numafile(filename,nline)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(out) :: nline

  integer ok, iuf

  call checkfile(trim(filename))
  nline=0
  open(newunit=iuf,file=trim(filename),status='old',iostat = ok)
  do while(ok == 0)
    read(iuf,*,iostat = ok)
    if (ok == 0) nline=nline+1
  enddo
  close(iuf)

  return

end subroutine numafile

!> numaxyzfile: subroutine numaxyzfile(filename,natm)
!> numaxyzfile: Depends on checkfile
!> numaxyzfile: Counts the number of atoms (natm) in a xyzfile with filename filename
subroutine numaxyzfile(filename,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(out) :: natm

  integer iuf

  call checkfile(trim(filename))
  open(newunit=iuf,file=trim(filename))
    read(iuf,*) natm
  close(iuf)

  return

end subroutine numaxyzfile

!> numamultixyzfile: subroutine numamultixyzfile(filename,natm,ngeo)
!> numamultixyzfile: Depends on checkfile, numaxyzfile, numafile
!> numamultixyzfile: Counts the number of atoms (natm) and geometries (ngeo) in a multi-xyzfile with filename filename
subroutine numamultixyzfile(filename,natm,ngeo)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(out) :: natm, ngeo

  integer nline

  call checkfile(trim(filename))
  call numaxyzfile(trim(filename),natm)
  call numafile(trim(filename),nline)
  ngeo=nline/(natm+3)
  if ( ngeo == 0 ) ngeo = 1

  return

end subroutine numamultixyzfile

!> numamolfile: subroutine numamolfile(filename,natm,nbnd)
!> numamolfile: Depends on checkfile
!> numamolfile: Counts the number of atoms (natm) and the number of bonds (nbnd) in a .mol formatted file with filename filename
subroutine numamolfile(filename,natm,nbnd)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(out) :: natm, nbnd

  integer iuf

  call checkfile(trim(filename))
  open(newunit=iuf,file=trim(filename))
    read(iuf,*)
    read(iuf,*)
    read(iuf,*)
    read(iuf,'(2i3)') natm, nbnd
  close(iuf)

  return

end subroutine numamolfile

!> readxyzfile: subroutine readxyzfile(filename,atm,cmt,natm)
!> readxyzfile: Reads the list of atom labels and cartesian coordinates of the xyzfile with filename filename
!> readxyzfile: and stores them in the 1-dimensional and 2-dimensional arrays atm and cmt, respectively
!> readxyzfile: the number of atoms (natm) must be given
subroutine readxyzfile(filename,atm,cmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  character*(*), intent(out) :: atm(natm)
  double precision, intent(out) :: cmt(natm,3)

  integer i, j, iuf

  open(newunit=iuf,file=trim(filename))
    read(iuf,*)
    read(iuf,*)
    do i=1, natm
      read(iuf,*) atm(i), (cmt(i,j), j=1,3)
    enddo
  close(iuf)

  return

end subroutine readxyzfile

!> readmultixyzfile: subroutine readmultixyzfile(filename,atm,cmt,natm,ngeo)
!> readmultixyzfile: Reads (and checks) the list of atom labels and cartesian coordinates of the multi-xyzfile with filename filename
!> readmultixyzfile: and stores them in the 1-dimensional and 3-dimensional arrays atm and cmt, respectively
!> readxyzfile: the number of atoms (natm) and the number of geometries (ngeo) must be given
subroutine readmultixyzfile(filename,atm,cmt,natm,ngeo)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm, ngeo
  character*(*), intent(out) :: atm(natm)
  double precision, intent(out) :: cmt(ngeo,natm,3)

  integer i, j, k, iuf
  character*2 atmtmp(natm)

  open(newunit=iuf,file=trim(filename))
    do k=1, ngeo
      read(iuf,*)
      read(iuf,*)
      do i=1, natm
        read(iuf,*) atm(i), (cmt(k,i,j), j=1,3)
        if ( k /= 1 .and. atm(i) /= atmtmp(i) ) then
          write(*,*)
          write(*,*) "Error!!! Inconsistent sequence of atoms!"
          write(*,*)
          stop
        endif
      enddo
      atmtmp=atm
    enddo
  close(iuf)

  return

end subroutine readmultixyzfile


!> readzmtfile1: subroutine readzmtfile1(filename,atm,na,zmt,natm)
!> readzmtfile1: Reads the list of atom labels and the Z-matrix of the Z-matrix file with filename filename
!> readzmtfile1: and stores them in three different arrays:
!> readzmtfile1: atm is a 1-dimensional array which contains the atom labels
!> readzmtfile1: na is a 2-dimensional array whose rows contain sequences of 3 atom indexes to which each atom (defined by the row index) is connected (and unequivocally defined)
!> readzmtfile1: zmt contains the values of the bond length, bond angle and dihedral angle defined by na
!> readzmtfile1: the number of atoms (natm) must be given
subroutine readzmtfile1(filename,atm,na,zmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  natm
  character*(*), intent(out) :: atm(natm)
  integer, intent(out) ::  na(natm,3)
  double precision, intent(out) :: zmt(natm,3)

  integer i, j, iuf

  open(newunit=iuf,file=trim(filename))
  read(iuf,*)
  read(iuf,*)
    do i=1, natm
      read(iuf,*) atm(i), (na(i,j), j=1, 3), (zmt(i,j), j=1,3)
    enddo
  close(iuf)

  return

end subroutine readzmtfile1

!> readzmtfile2: subroutine readzmtfile2(filename,atm,na,zmt,natm)
!> readzmtfile2: Same as readzmtfile1 but in the case where atom labels are indices (i.e., integer labels) such as defined in Siesta
subroutine readzmtfile2(filename,atm,na,zmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  natm
  integer, intent(out) ::  atm(natm), na(natm,3)
  double precision, intent(out) :: zmt(natm,3)

  integer i, j, iuf

  open(newunit=iuf,file=trim(filename))
    do i=1, natm
      read(iuf,*) atm(i), (na(i,j), j=1, 3), (zmt(i,j), j=1,3)
    enddo
  close(iuf)

  return

end subroutine readzmtfile2

!> readnamov: subroutine readnamov(filename,na,amov,natm)
!> readnamov: Reads the sequence of atom indices (na) and the sequence of amov options from a filename filename
!> readnamov: As for the Z-matrix, na is a 2-dimensional array whose rows contain sequences of 3 atom indices
!> readnamov: amov is a 2-dimensional array which contains values equal to 0 or 1 only, defining for each atom (row),
!> readnamov: the freezing or the release of the corresponding bond stretch, angle bending and torsion as defined in na
subroutine readnamov(filename,na,amov,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  natm
  integer, intent(out) :: na(natm,3), amov(natm,3)

  integer i, j, iuf
  integer i_tmp
  double precision d_tmp

  open(newunit=iuf,file=trim(filename))
    do i=1, natm
      read(iuf,*) i_tmp, (na(i,j), j=1, 3), (d_tmp, j=1,3), (amov(i,j), j=1, 3)
    enddo
  close(iuf)

  return

end subroutine readnamov

!> readmolfile: subroutine readmolfile(filename,cmt,atm,bnd,natm,nbnd)
!> readmolfile: Reads the cartesian coordinates, the list of atom labels and the bond connections of the .mol formatted file with filename filename
!> readmolfile: and stores them in the 2-dimensional, 1-dimensional and 2-dimensional arrays cmt, atm and bnd, respectively
!> readmolfile: the number of atoms (natm) and the number of bonds (nbnd) must be given
subroutine readmolfile(filename,cmt,atm,bnd,natm,nbnd)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  natm, nbnd
  double precision, intent(out) :: cmt(natm,3)
  character*(*), intent(out) :: atm(natm)
  integer, intent(out) ::  bnd(nbnd,3)


  integer i, j, iuf

  open(newunit=iuf,file=trim(filename))
    read(iuf,*)
    read(iuf,*)
    read(iuf,*)
    read(iuf,*)
    do i=1, natm
      read(iuf,*) (cmt(i,j), j=1,3), atm(i)
    enddo
    do i=1, nbnd
      read(iuf,'(3i3)') (bnd(i,j), j=1, 3)
    enddo
  close(iuf)

  return

end subroutine readmolfile



!> readbndfile: subroutine readbndfile(filename,bnd,nbnd)
!> readbndfile: Reads the bond connections from the file with filename filename
!> readbndfile: bnd is a 2-dimensional array which list the bonds and whose row give the indices of two bounded atoms (column 1 and 2) and the type of bond (e.g., single, double, etc. bond - column 3)
!> readbndfile: the number of bonds (nbnd) must be given
subroutine readbndfile(filename,bnd,nbnd)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  nbnd
  integer, intent(out) ::  bnd(nbnd,3)


  integer i, j, iuf

  open(newunit=iuf,file=trim(filename))
    do i=1, nbnd
      read(iuf,'(3i3)') (bnd(i,j), j=1, 3)
    enddo
  close(iuf)

  return

end subroutine readbndfile

!> readtokfile: subroutine readtokfile(filename,na,tok,ntok,opt)
!> readtokfile: Reads the sequence of atom indices (na) and the force constants from a filename filename
!> readtokfile: As for the Z-matrix, na is a 2-dimensional array whose rows contain sequences of atom indices defining a bond, a bond angle or a torsion
!> readtokfile: It is worth to notice that contrary to the Z-matrix, na may contained a variable number of indices per row (defined by the given integer opt)
!> readtokfile: and that the index of the first atom is not implicit anymore (i.e., does not correspond to the row index) but is specifically defined in the first column of na
!> readtokfile: tok is a 1-dimensional array containing the force constants corresponding to the bond length, bond angle or dihedral angle defined by na
!> readtokfile: The number of force constants (ntok) must be given
subroutine readtokfile(filename,na,tok,ntok,opt)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) ::  ntok, opt
  integer, intent(out) ::  na(ntok,opt+1)
  double precision, intent(out) ::  tok(ntok)

  integer i, j, iuf
  double precision d_tmp

  open(newunit=iuf,file=trim(filename))
  do i=1, ntok
     read(iuf,*) (na(i,j), j=1, opt+1), d_tmp, tok(i)
  enddo
  close(iuf)

  return

end subroutine readtokfile

!> readtokfile2: subroutine readtokfile2(filename,alist,na,ntok,opt)
!> readtokfile2: Same as readtokfile but storing the atom indices in an alternative way:
!> readtokfile2: alist is a 1-dimensional array containing now the indices of the first atoms
!> readtokfile2: na is a 2-dimensional array containing the sequence of atom indices to which the first atoms are connected to
subroutine readtokfile2(filename,alist,na,ntok,opt)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: ntok, opt
  integer, intent(out) ::  alist(ntok), na(ntok,3)

  integer i, j, iuf
  double precision d_tmp

  open(newunit=iuf,file=trim(filename))
  do i=1, ntok
     read(iuf,*) alist(i), (na(i,j), j=1, opt)
  enddo
  close(iuf)

  j=opt+1
  do while ( j <= 3 )
    do i=1, ntok
      na(i,j)=minval(na(i,1:opt))-1
    enddo
    j=j+1
  enddo

  return

end subroutine readtokfile2

!> writexyzfile: subroutine writexyzfile(filename,atm,cmt,natm)
!> writexyzfile: Writes the list of atom labels (atm) and the cartesian coordinates (cmt) in a file with filename filename and extension ".xyz"
!> writexyzfile: The number of atoms (natm) must be given
subroutine writexyzfile(filename,atm,cmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  character*(*), intent(in) :: atm(natm)
  double precision, intent(in) ::  cmt(natm,3)

  integer i, j, iuf

  if ( trim(filename) == "" ) then
    write(*,'(i4)') natm
    write(*,*)
    do i=1, natm
      write(*,'(a2,3f16.8)') atm(i), (cmt(i,j), j=1,3)
    enddo
  else
    open(newunit=iuf,file=filename)
      write(iuf,'(i4)') natm
      write(iuf,'(a,f16.7)')
      do i=1, natm
	write(iuf,'(a2,3f16.8)') atm(i), (cmt(i,j), j=1,3)
      enddo
    close(iuf)
  endif

end subroutine writexyzfile


!> writexyzfile2: subroutine writexyzfile2(filename,atm,cmt,natm)
!> writexyzfile2: Sames as writexyzfile but when the atom labels (atm) are integers.
subroutine writexyzfile2(filename,atm,cmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  integer, intent(in) :: atm(natm)
  double precision, intent(in) ::  cmt(natm,3)

  integer i, j, iuf

  if ( trim(filename) == "" ) then
    write(*,'(i4)') natm
    write(*,*)
    do i=1, natm
      write(*,'(i6,3f16.8)') atm(i), (cmt(i,j), j=1,3)
    enddo
  else
    open(newunit=iuf,file=filename)
      write(iuf,'(i4)') natm
      write(iuf,'(a,f16.7)')
      do i=1, natm
	write(iuf,'(i6,3f16.8)') atm(i), (cmt(i,j), j=1,3)
      enddo
    close(iuf)
  endif

end subroutine writexyzfile2


!> writemultixyzfile: subroutine writemultixyzfile(iuf,atm,cmt,natm)
!> writemultixyzfile: Writes the list of atom labels (atm) and the cartesian coordinates (cmt) for each geometry in a file with filename filename and extension ".XYZ"
!> writemultixyzfile: The number of atoms (natm) and the number of geometries (ngeo) must be given
subroutine writemultixyzfile(iuf,atm,cmt,natm)

  implicit none

  integer, intent(in) :: iuf
  integer, intent(in) :: natm
  character*(*), intent(in) :: atm(natm)
  double precision, intent(in) ::  cmt(natm,3)

  integer i, j

  write(iuf,'(i4)') natm
  write(iuf,*)
  do i=1, natm
    write(iuf,'(a2,3f16.8)') atm(i), (cmt(i,j), j=1,3)
  enddo
  write(iuf,*)

end subroutine writemultixyzfile



!> writemolfile: subroutine writemolfile(filename,atm,cmt,bnd,natm,nbnd)
!> writemolfile: Writes the list of atom labels (atm), the cartesian coordinates (cmt) and the bond connections (bnd) in a file with filename filename and extension ".mol"
!> writemolfile: The number of atoms (natm) and the number of bonds (nbnd) must be given
subroutine writemolfile(filename,atm,cmt,bnd,natm,nbnd)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm, nbnd
  character*(*), intent(in) :: atm(natm)
  double precision, intent(in) ::  cmt(natm,3)
  integer, intent(in) ::  bnd(nbnd,3)

  integer i, j, iuf
  character*50 labmol, header, footer
  
  
  labmol="writemolfile"
  header="  0  0  0  0  0  0  0  0999 V2000"
  footer="M  END"

  if ( trim(filename) == "" ) then
    write(*,*)
    write(*,'(a)') trim(labmol)
    write(*,*)
    write(*,'(2i3,a)') natm, nbnd, trim(header)
    do i=1, natm
      write(*,'(3f10.4,a3,12i3)') (cmt(i,j), j=1,3), atm(i), (0, j=1, 12)
    enddo
    do i=1, nbnd
      write(*,'(7i3)') (bnd(i,j), j=1,3), (0, j=1, 4)
    enddo
    write(*,'(a)') trim(footer)
  else
    open(newunit=iuf,file=filename)
      write(iuf,*)
      write(iuf,'(a)') trim(labmol)
      write(iuf,*)
      write(iuf,'(2i3,a)') natm, nbnd, trim(header)
      do i=1, natm
	write(iuf,'(3f10.4,a3,12i3)') (cmt(i,j), j=1,3), atm(i), (0, j=1, 12)
      enddo
      do i=1, nbnd
	write(iuf,'(7i3)') (bnd(i,j), j=1,3), (0, j=1, 4)
      enddo
      write(iuf,'(a)') trim(footer)
    close(iuf)
  endif

end subroutine writemolfile


!> writezmtfile1: subroutine writezmtfile1(filename,atm,na,zmt,natm)
!> writezmtfile1: Writes the list of atom labels (atm) and the Z-matrix (sequence of atom indices defining the bond, bond angle and torsion (na) and their corresponding values (zmt))
!> writezmtfile1: in a file with filename filename and extension ".zmt"
subroutine writezmtfile1(filename,atm,na,zmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  character*(*), intent(in) :: atm(natm)
  integer, intent(in) :: na(natm,3)
  double precision, intent(in) :: zmt(natm,3)

  integer i, j, iuf
  
  if ( trim(filename) == "" ) then
    write(*,'(i6)') natm
    write(*,*)
    do i=1, natm
      write(*,'(a2,3i6,3f16.8)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3)
    enddo
  else
    open(newunit=iuf,file=filename)
      write(iuf,'(i6)') natm
      write(iuf,*)
      do i=1, natm
	write(iuf,'(a2,3i6,3f16.8)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3)
      enddo
    close(iuf)
  endif

end subroutine writezmtfile1

!> writezmtfile2: subroutine writezmtfile2(filename,atm,na,zmt,natm)
!> writezmtfile2: Same as writezmtfile1 but in the case where atom labels correspond to specie indices (i.e., integer labels) such as defined in Siesta
subroutine writezmtfile2(filename,atm,na,zmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  integer, intent(in) :: atm(natm), na(natm,3)
  double precision, intent(in) :: zmt(natm,3)

  integer i, j, iuf
  
  if ( trim(filename) == "" ) then
    do i=1, natm
      write(*,'(i6, 3i6,3f16.8)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3)
    enddo
  else
    open(newunit=iuf,file=filename)
      do i=1, natm
	write(iuf,'(i6, 3i6,3f16.8)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3)
      enddo
    close(iuf)
  endif

end subroutine writezmtfile2

!> writezmtfile3: subroutine writezmtfile3(filename,atm,na,amov,zmt,natm)
!> writezmtfile3: Same as writezmtfile1 but in the case where atom labels are indices (i.e., integer labels)
!> writezmtfile3: and adding the freezing/release values (amov) for the bond stretch, angle bending and torsion defined for each atom by na
!> writezmtfile3: This subroutine permits to write Z-matrix in a format readable by Siesta
subroutine writezmtfile3(filename,atm,na,amov,zmt,natm)

  implicit none

  character*(*), intent(in) :: filename
  integer, intent(in) :: natm
  integer, intent(in) :: atm(natm), na(natm,3), amov(natm,3)
  double precision, intent(in) :: zmt(natm,3)

  integer i, j, iuf

  if ( trim(filename) == "" ) then
    do i=1, natm
      write(*,'(i6, 3i6,3f16.8,3i6)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3), (amov(i,j), j=1,3)
    enddo
  else
    open(newunit=iuf,file=filename)
      do i=1, natm
	write(iuf,'(i6, 3i6,3f16.8,3i6)') atm(i), (na(i,j), j=1,3), (zmt(i,j), j=1,3), (amov(i,j), j=1,3)
      enddo
    close(iuf)
  endif

end subroutine writezmtfile3

!> uniqtab: subroutine uniqtab(tab,nr)
!> uniqtab: Deletes the repetitive values in the 1-dimensional array tab and reshapes it such that the remaining unique values corresponds to the first nr elements
!> uniqtab: The size of tab (nr) must be given and will be return reduced by the number of doublons
subroutine uniqtab(tab,nr)

  implicit none

  integer, intent(inout) :: nr
  integer, intent(inout) :: tab(nr)

  integer i, j, k

  i=1
  do while ( i <= nr-1 )
    do j=nr, i+1, -1
      if ( tab(i) == tab(j) ) then
        nr=nr-1
        do k=j, nr
          tab(k)=tab(k+1)
        enddo
      endif
    enddo
    i=i+1
  enddo

end subroutine uniqtab

!> sortmat: subroutine sortmat(mat,nr,nc,ic,op)
!> sortmat: Sort the 2-dimensional array mat according to the column ic.
!> sortmat: op can only take the values -1 or 1, which define the decreasing and increasing order, respectively
!> sortmat: The number of rows (nr) and the number of columns (nc) must be given
  subroutine sortmat(mat,nr,nc,ic,op)

  implicit none

  integer, intent(in) :: nr, nc, ic, op
  integer, intent(inout) :: mat(nr,nc)

  integer i, j
  integer matmp(nc)

  select case (op)

  ! Decreasing order
  case(-1)

    do i=1,nr-1
      do j=i+1,nr
        if (mat(j,ic)>mat(i,ic)) then
          matmp=mat(i,:)
          mat(i,:)=mat(j,:)
          mat(j,:)=matmp
        endif
      enddo
    enddo

  ! Increasing order
  case(1)

    do i=1,nr-1
      do j=i+1,nr
        if (mat(j,ic)<mat(i,ic)) then
          matmp=mat(i,:)
          mat(i,:)=mat(j,:)
          mat(j,:)=matmp
        endif
      enddo
    enddo

  case default

    write(*,*)
    write(*,*) "Error!!! Bad argument for option op: op=", op
    write(*,*)
    stop

  end select

  return

end subroutine sortmat

!> xyzbndreorder: subroutine xyzbndreorder(cmt,atm,bnd,alist,inid,natm,nbnd)
!> xyzbndreorder: Depends on reordxyz
!> xyzbndreorder: Reorders the list of atoms (atom labels atm and cartesian coordinates (cmt) as a function of the bonds (defined by the array bnd) and the index of an initial atom (inid)
!> xyzbndreorder: Working principles: the atom with index inid will be taken as the first atom. The atoms that will follow in the new list are the ones which are bounded to this initial atom.
!> xyzbndreorder: If no bounded atoms are found, the first atom from the original list of atoms and not already defined in the new list of atoms will be taken.
!> xyzbndreorder: The rest of the new list of atoms will be completed in the same way.
!> xyzbndreorder: alist is a 1-dimensional array containing the order of the original list of atoms. For normal cases, it should be equal to (/1,2,...natm/)
!> xyzbndreorder: bnd is a 2-dimensional array which list the bonds and whose rows give the indices of two bounded atoms (column 1 and 2) and the type of bond (e.g., single, double, etc. bond - column 3)
!> xyzbndreorder: The number of atoms (natm) and the number of bounds (nbnd) must be given
  subroutine xyzbndreorder(cmt,atm,bnd,alist,inid,natm,nbnd)

    implicit none

    integer, intent(in) :: natm, nbnd, inid
    integer, intent(in) :: bnd(nbnd,3)
    integer, intent(inout) :: alist(natm)
    character*(*), intent(inout) :: atm(natm)
    double precision, intent(inout) :: cmt(natm,3)

    integer i, j, k, ko
    integer blist(natm), clist(natm)

    blist=0
    !% Checks that the initial atom index is in the original list of atoms
    if ( ANY(alist==inid ) ) then
      blist(1)=inid
    else
      write(*,*)
      write(*,*)
      write(*,*) "Error!!! Initial index out of range!"
      write(*,*) "inid=", inid
      write(*,*)
      stop
    endif
    
    k=1
    !% Loop on the index of the original list of atoms
    do i=1, natm
      ko=k
      !% Loop on the index of the bond list
      do j=1, nbnd
	!% Checks that the atom indices in the bond list are in the original list of atoms
        if ( ANY(alist==bnd(j,1)) .and. ANY(alist==bnd(j,2)) ) then
	  !% If one of the atom indices in the bond list is equal to the current atom index in the new list and not already defined in the new list of atoms,
	  !% add it to the new list and increase the index k of the new list of atoms
          if ( bnd(j,2) == blist(i) .and. .not. ANY(blist==bnd(j,1)) ) then
            k=k+1
            if ( k <= i .or. k > natm ) then
              write(*,*)
              write(*,*)
              write(*,*) "Error!!! Index of reordered list is out of range."
              write(*,*) "i, natm, k=", i, natm, k
              write(*,*)
              stop
            endif
            blist(k)=bnd(j,1)
          endif
          if ( bnd(j,1) == blist(i) .and. .not. ANY(blist==bnd(j,2)) ) then
            k=k+1
            if ( k <= i .or. k > natm ) then
              write(*,*)
              write(*,*)
              write(*,*) "Error!!! Index of reordered list is out of range."
              write(*,*) "i, natm, k=", i, natm, k
              write(*,*)
              stop
            endif
            blist(k)=bnd(j,2)
          endif
        endif
      enddo
      
      !% If no bounded atom has been found and if it is not the last atom in the new list of atoms,
      !% add the first atom from the original list which is not already defined in the new list of atoms
      if ( ko == k .and. i == k .and. i /= natm) then
        k=k+1
        do j=1, natm
          if ( .not. ANY(blist==alist(j)) ) then
            blist(k)=alist(j)
            exit
          endif
        enddo
      endif
    enddo
    
    !% Checks that the whole list has been reordered
    if ( k /= natm ) then
      write(*,*)
      write(*,*)
      write(*,*) "Error!!! Index of reordered list not equal to natm."
      write(*,*) "natm, k=", natm, k
      write(*,*)
      stop
    endif

    !% Creates a list clist which links the elements of blist to the elements of alist
    do i=1, natm
      k=0
      do j=1, natm
        if ( blist(i) == alist(j) ) then
          clist(i)=j
          k=k+1
        endif
      enddo
      if ( k /= 1 ) then
        write(*,*)
        write(*,*)
        write(*,*) "Error!!! Inconsistent atom lists."
        write(*,*) "i, k=", i, k
        write(*,*)
        stop
      endif
    enddo

    alist=blist
    
    !% Reoreders the atom labels and the cartesian coordinates from the list clist
    call reordxyz(cmt,atm,natm,clist,natm)


    return

  end subroutine xyzbndreorder

!> reordxyz: subroutine reordxyz(cmt,atm,natm,olist,nol)
!> reordxyz: Reorders the atom labels (atm) and the cartesian coordinates (cmt) from a list of atom indices (olist)
!> reordxyz: olist is a 1-dimensional array giving the new order of the atoms (referenced by the row index). The size of olist can be lower than the number of atoms (natm) of atm and cmt. 
!> reordxyz: In such case, only the atom indices contained in olist will be reorederd and placed at the beginning of the new list of atoms.
!> reordxyz: The number of atoms (natm) of atm and cmt and the size of olist (nol) must be given
subroutine reordxyz(cmt,atm,natm,olist,nol)

  implicit none


  integer, intent(in) :: natm, nol
  integer, intent(in) :: olist(nol)
  double precision, intent(inout) :: cmt(natm,3)
  character*(*), intent(inout) :: atm(natm)

  integer i, j, k, kount
  integer flist(natm)
  character*2 atmtmp(natm)
  double precision cmtmp(natm,3)


  if ( nol > natm ) then
    write(*,*)
    write(*,*) "Error!!! Ordering list is bigger than xyz matrix!"
    write(*,*)
    stop
  endif

  !% Checks that all the elements of olist are contained in the range 1:natm
  kount=0
  do i=1, natm
    do j=1, nol
      if ( i == olist(j) ) then
        kount=kount+1
      endif
    enddo
  enddo

  if ( kount /= nol ) then
    write(*,*)
    write(*,*) "Error!!! Ordering list is corrupted!"
    write(*,*)
    stop
  endif

  !% Build a new ordering list in the case where the size of olist is lower than natm. olist is first copied at the beginning of flist.
  !% The rest of flist is then completed by all the atom indices which are not contained in olist.
  do i=1, nol
    flist(i)=olist(i)
  enddo
  k=nol+1
  do i=1, natm
    if ( .not. ANY(olist==i) ) then
      flist(k)=i
      k=k+1
    endif
  enddo

  !% Reorders atm and cmt according to flist
  do i=1, natm
    cmtmp(i,:)=cmt(flist(i),:)
    atmtmp(i)=atm(flist(i))
  enddo

  cmt=cmtmp
  atm=atmtmp

  return

end subroutine reordxyz


!> reordbnd: subroutine reordbnd(bnd,nbnd,olist,nol)
!> reordbnd: Reorders a 2-dimensional array containing the bond specifications (bnd) according to an ordering list (olist)
!> reordbnd: The number of bonds (nbnd) and the size of olist (nol) must be given
subroutine reordbnd(bnd,nbnd,olist,nol)

  implicit none
  
  integer, intent(in) :: nbnd, nol
  integer, intent(in) :: olist(nol)
  integer, intent(inout) :: bnd(nbnd,3)
  
  integer i, j
  integer flist(nol)
  
  do i=1, nol
    flist(olist(i))=i
  enddo
  
  do i=1, nbnd
    do j=1, 2
      bnd(i,j)=flist(bnd(i,j))
    enddo
  enddo
  
end subroutine reordbnd



!> nearestat: subroutine nearestat(iati,iatm,iatn,natm,cmt)
!> nearestat: Gives the indices of the nearest atoms (itan) to the atoms defined by the indices in iati, after the atoms defined by the indices in iatm.
!> nearestat: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given.
subroutine nearestat(iati,iatm,iatn,natm,cmt)

  implicit none

  integer, intent(in) :: iati, iatm, natm
  double precision, intent(in) :: cmt(natm,3)
  integer, intent(out) :: iatn

  integer i, j
  double precision dist, dist0, distm
  double precision dc(3)

  dc=(cmt(iati,:)-cmt(iatm,:))**2
  distm=dsqrt(dc(1)+dc(2)+dc(3))

  iatn=0
  dist0=1000000.d0

  do i=1, natm
    if ( i /= iati ) then
      do j=1, 3
        dc(j)=cmt(iati,j)-cmt(i,j)
        dc(j)=dc(j)*dc(j)
      enddo
      dist=dsqrt(dc(1)+dc(2)+dc(3))
      if ( dist < dist0 .and. dist >= distm .and. i /= iatm ) then
        dist0=dist
        iatn=i
      endif
    endif
  enddo

  return

end subroutine nearestat

!> nearestat2: subroutine nearestat2(cati,iatn,natm,cmt)
!> nearestat2: Gives the indices of the nearest atoms (itan) to the caratesian coordinates cati.
!> nearestat2: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given.
subroutine nearestat2(cati,iatn,natm,cmt)

  implicit none

  integer, intent(in) :: natm
  double precision, intent(in) :: cati(3), cmt(natm,3)
  integer, intent(out) :: iatn

  integer i, j
  double precision dist, dist0, distm
  double precision dc(3)

  iatn=0
  dist0=1000000.d0
  do i=1, natm
      do j=1, 3
        dc(j)=cati(j)-cmt(i,j)
        dc(j)=dc(j)*dc(j)
      enddo
      dist=dsqrt(dc(1)+dc(2)+dc(3))
      if ( dist < dist0  ) then
        dist0=dist
        iatn=i
      endif
  enddo

  if ( iatn == 0 ) then
    write(*,*)
    write(*,*) "Error!!! No nearest atom found!"
    write(*,*)
    stop
  endif

  return

end subroutine nearestat2

!> cart2sph: subroutine cart2sph(cmt,natm,amode,op)
!> cart2sph: Converts cartesian coordinates <-> spherical coordinates.
!> cart2sph: cmt is a 2-dimensional array containing the cartesian coordinates or the spherical coordinates
!> cart2sph: amode (= "degree" or "radian") defines the unit of the angles
!> cart2sph: op (=-1 or 1) defines the conversion direction: -1) cartesian coordinates -> spherical coordinates ; 1) spherical coordinates -> cartesian coordinates
!> cart2sph: The number of atoms (natm) must be given
subroutine cart2sph(cmt,natm,amode,op)

  implicit none

  integer, intent(in) :: natm, op
  character*(*), intent(in) :: amode
  double precision, intent(inout) :: cmt(natm,3)

  integer i, facang
  double precision xR, ytheta, zphi
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree") then
    facang=pig/180.d0
  else
    if ( amode == "radian" ) then
      facang=1.d0
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif

  select case(op)
  !% cartesian coordinates -> spherical coordinates
  case (1)
    do i=1, natm
      xR=cmt(i,1)
      ytheta=cmt(i,2)
      zphi=cmt(i,3)
      cmt(i,1)=dsqrt(xR**2+ytheta**2+zphi**2)
      if ( xR == 0.d0 ) then
        if ( ytheta == 0.d0 ) then
          cmt(i,3)=0.d0
        else
          cmt(i,3)=pig/2.d0
        endif
      else
	cmt(i,3)=datan(ytheta/xR)
	if ( xR < 0.d0 ) then
	  cmt(i,3)=cmt(i,3)+pig
	endif
      endif
      if ( cmt(i,1) == 0.d0 ) then
        cmt(i,2)=0.d0
      else
        cmt(i,2)=dacos(zphi/cmt(i,1))
      endif
      cmt(i,2:3)=cmt(i,2:3)*facang
    enddo
  
  !% spherical coordinates -> cartesian coordinates
  case(-1)
    do i=1, natm
      xR=cmt(i,1)
      ytheta=cmt(i,2)*facang
      zphi=cmt(i,3)*facang
      cmt(i,1)=xR*dsin(ytheta)*dcos(zphi)
      cmt(i,2)=xR*dsin(ytheta)*dsin(zphi)
      cmt(i,3)=xR*dcos(ytheta)
    enddo

  case DEFAULT
    write(*,*)
    write(*,*) "Error! Bad op option in cart2sph!"
    write(*,*)
    stop

  end select

  return

end subroutine cart2sph

!> rotmatgen: subroutine rotmatgen(RM,ind,ang)
!> rotmatgen: Generates the rotation matrix (RM) for the axes x, y or z defined by the index (ind=1, 2, 3 respectively) and for the angle ang (in radian)
subroutine rotmatgen(RM,ind,ang)

  implicit none

  integer, intent(in) :: ind
  double precision, intent(in) :: ang
  double precision, intent(out) :: RM(3,3)

  RM=0.d0
  select case (ind)

  case (1)
    RM(1,1)=1.d0
    RM(2,2)=dcos(ang)
    RM(2,3)=-dsin(ang)
    RM(3,2)=dsin(ang)
    RM(3,3)=dcos(ang)

  case(2)
    RM(1,1)=dcos(ang)
    RM(1,3)=dsin(ang)
    RM(2,2)=1.d0
    RM(3,1)=-dsin(ang)
    RM(3,3)=dcos(ang)

  case(3)
    RM(1,1)=dcos(ang)
    RM(1,2)=-dsin(ang)
    RM(2,1)=dsin(ang)
    RM(2,2)=dcos(ang)
    RM(3,3)=1.d0

  case default
    write(*,*)
    write(*,*) "Error! Bad option for rotation matrix."
    write(*,*)
    stop

  end select

  return

end subroutine rotmatgen

!> rotmatavec: subroutine rotmatavec(cmt,natm,u1,u2)
!> rotmatavec: Depends on ramatgen
!> rotmatavec: Rotates the cartesian coordinates contained in the 2-dimensional array (cmt) by the angles needed to align the vector u1 with the vector u2
!> rotmatavec: The number of atoms (natm) must be given
subroutine rotmatavec(cmt,natm,u1,u2)

  use vecop

  implicit none

  integer, intent(in) :: natm
  double precision, intent(in) :: u1(3), u2(3)
  double precision, intent(inout) ::  cmt(natm,3)

  integer i, j
  double precision u1b(3), u2b(3), u2c(3), u3(3)
  double precision RM(3,3), xyz1(3), xyz2(3)
  logical testvec

  u1b=u1/normvec(u1)
  u2b=u2/normvec(u2)
  
  !% Computes the rotation matrix RM defined by the normalized vectors u1b and u2b
  call ramatgen(u1b,u2b,RM)

  do i=1, natm
    do j=1, 3
      xyz1(j)=cmt(i,j)
    enddo
    !% Rotates each row of the array of cartesian coordinates cmt
    xyz2=matmul(RM,xyz1)
    do j=1, 3
      cmt(i,j)=xyz2(j)
    enddo
  enddo

  return

end subroutine rotmatavec

!> ramatgen: subroutine ramatgen(u1,u2,RM)
!> ramatgen: Depends on normvec, crossprod
!> ramatgen: Computes the rotation matrix RM needed to align the vector u1 with the vector u2
subroutine ramatgen(u1,u2,RM)

  use vecop

  implicit none

  double precision, intent(in) :: u1(3), u2(3)
  double precision, intent(out) :: RM(3,3)

  integer i, j
  double precision sang, cang, fang
  double precision IM(3,3), u3(3), vx(3,3)

  IM=0.d0
  do i=1, 3
    IM(i,i)=1.d0
  enddo

  u3=crossprod(u1,u2)
  sang=normvec(u3)

  if ( sang == 0.d0 ) then
    !% Treats the specific case where u1 and u2 are collinears
    cang=dot_product(u1,u2)
    fang=dot_product(u1,u1)
    !% If u1 and u2 are collinear, the rotation matrix is equal to unity, If they are anticollinear, we have a problem.
    if ( cang == fang ) then
      RM=IM
    else
      write(*,*)
      write(*,*) "Error!!! Problem with anticollinear vectors!"
      write(*,*)
      stop
    endif

  else
    !% If u1 and u2 are not collinears, compute the rotation matrix in the usual way
    cang=dot_product(u1,u2)
    fang=(1.d0-cang)/(sang**2)

    vx=0.d0
    vx(1,2)=-u3(3)
    vx(1,3)=u3(2)
    vx(2,1)=u3(3)
    vx(2,3)=-u3(1)
    vx(3,1)=-u3(2)
    vx(3,2)=u3(1)

    RM=IM+vx+matmul(vx,vx)*fang
  endif

  return

end subroutine ramatgen

!> printmat: subroutine printmat(mat,nr,nc,lab)
!> printmat: Prints the matrix mat with a possible preliminary line containing a label lab
!> printmat: The number of rows (nr) and the number of columns (nc) must be given
subroutine printmat(mat,nr,nc,lab)

  implicit none

  integer, intent(in) :: nr, nc
  double precision, intent(in) :: mat(nr,nc)
  character*(*), intent(in) :: lab

  integer i, j

  if ( lab /= "nolab" ) then
    write(*,*)
    write(*,*) "Printing ", lab, ":"
  endif
  do i=1, nr
      if ( i == 1 ) then
        write(*,'(a10,999i10)') " ", (j, j=1, nc)
      endif
      write(*,'(i10,999f10.4)') i, (mat(i,j), j=1, nc)
  enddo

end subroutine printmat


!> centroid: subroutine centroid(cmt,natm,ind,nat,cat)
!> centroid: Compute the cartesian coordinates (cat) corresponding to the centroid defined by the atom indices (ind)
!> centroid: The 2-dimensional array of cartesian coordinates (cmt), the number of atoms (natm) and the number of atoms defining the centroid (nat), i.e., the size of ind, must be given
subroutine centroid(cmt,natm,ind,nat,cat)

  implicit none

  integer, intent(in) :: natm, nat
  double precision, intent(in) :: cmt(natm,3)
  integer, intent(in) :: ind(nat)
  double precision, intent(out) :: cat(3)
  integer i, j, k

  cat=0.d0
  do j=1, 3
    do i=1, nat
      k=ind(i)
      if ( k < 1 .or. k > natm ) then
        write(*,*)
        write(*,*) "Error!!! Bad index for atom in centroid subroutine: i, ind(i)=", i, k
        write(*,*)
        stop
      endif
      cat(j)=cat(j)+cmt(k,j)
    enddo
  enddo
  cat=cat/nat

  return

end subroutine centroid

!> trans2point: subroutine trans2point(cmt,natm,ind,cat)
!> trans2point: Translates the 2-dimensional array of cartesian coordinates (cmt) 
!> trans2point: by a vector equals to the difference of the cartesian coordinates of the translation vector (cat) and an atom defined by the index (ind)
!> trans2point: The number of atoms (natm) must be given
subroutine trans2point(cmt,natm,ind,cat)

  implicit none

  integer, intent(in) :: natm, ind
  double precision, intent(in) :: cat(3)
  double precision, intent(inout) :: cmt(natm,3)

  integer i
  double precision dvec(3)

  dvec=cat-cmt(ind,:)

  do i=1, natm
    cmt(i,:)=cmt(i,:)+dvec
  enddo

  return

end subroutine trans2point

!> trans2point2: subroutine trans2point2(cmt,natm,cat,cat0)
!> trans2point2: Translates the array of cartesian coordinates (cmt) by a vector equals to the difference of the cartesian coordinates of the translation vector (cat) and the spatial coordinates (cat0)
!> trans2point2: The number of atoms (natm) must be given
subroutine trans2point2(cmt,natm,cat,cat0)

  implicit none

  integer, intent(in) :: natm
  double precision, intent(in) :: cat(3), cat0(3)
  double precision, intent(inout) :: cmt(natm,3)

  integer i
  double precision dvec(3)

  dvec=cat0-cat

  do i=1, natm
    cmt(i,:)=cmt(i,:)+dvec
  enddo

  return

end subroutine trans2point2

!> checknummat: subroutine checknummat(mat1,mat2,nr,nc,tol)
!> checknummat: Check that the differences between two matrices (mat1 and mat2) is lower than a tolerance define by an exponent (tol)
!> checknummat: The number of rows (nr) and the number of columns (nc) must be given
subroutine checknummat(mat1,mat2,nr,nc,tol)

  implicit none

  integer, intent(in) :: nr, nc
  double precision, intent(in) :: mat1(nr,nc), mat2(nr,nc)
  double precision, intent(in) :: tol

  integer i, j

  do i=1, nr
    do j=1, nc
      if ( nint((mat1(i,j)-mat2(i,j))*10.d0**tol) .ne. 0 ) then
        write(*,*)
        write(*,*) "Warning! Different values in matrices:"
        write(*,*) "i, j, mat1(i,j),mat2(i,j)=", i, j, mat1(i,j),mat2(i,j)
      endif
    enddo
  enddo

end subroutine checknummat

!> convxyz2zmt: subroutine convxyz2zmt(cmt,natm,na,zmt,opt)
!> convxyz2zmt: Depends on reordna, zmtline
!> convxyz2zmt: Converts a 2-dimensional array of cartesian coordinates (cmt) into a Z-matrix (na and zmt)
!> convxyz2zmt: na is a 2-dimensional array whose rows contain sequences of 3 atom indices defining a bond, a bond angle or a torsion
!> convxyz2zmt: opt=1 or 2 ; when op=1, na is evaluated automatically while when op=2, na is taken as input
!> convxyz2zmt: zmt is a 2-dimensional array containing the values of the bond length, bond angle and dihedral angle defined by na.
!> convxyz2zmt: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given
subroutine convxyz2zmt(cmt,natm,na,zmt,opt)

  implicit none

  integer, intent(in) :: natm, opt
  double precision, intent(in) ::  cmt(natm,3)
  integer, intent(inout) ::  na(natm,3)
  double precision, intent(out) ::  zmt(natm,3)

  integer i, j

  select case(opt)

  case (1)
    !% Calls the subroutine to evaluate na
    call reordna(cmt,natm,na)
    do i=1, natm
      call zmtline(cmt,natm,i,na(i,:),zmt(i,:))
    enddo

  case(2)
    !% na is taken as input
    do i=1, natm
      call zmtline(cmt,natm,i,na(i,:),zmt(i,:))
    enddo

  case DEFAULT
    write(*,*)
    write(*,*) "Error! Bad argument for opt option in convxyz2zmt: opt=", opt
    write(*,*)
    stop

  end select

  return

 end subroutine convxyz2zmt

!> dihedrang: subroutine dihedrang(cmt,natm,inda,na,dha)
!> dihedrang: Depends on crossprod, normvec
!> dihedrang: Evaluates the dihedral angle (dha) defined by the atom index inda and the sequence of 3 atom indices contained in na
!> dihedrang: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given
subroutine dihedrang(cmt,natm,inda,na,dha)

  use vecop

  implicit none

  integer, intent(in) :: natm, inda
  double precision, intent(in) :: cmt(natm,3)
  integer, intent(in) :: na(3)
  double precision, intent(out) :: dha

  double precision b1(3), b2(3), b3(3)
  double precision n1(3), n2(3), n3(3)
  double precision ax, ay
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  b1=cmt(na(1),:)-cmt(inda,:)
  b2=cmt(na(2),:)-cmt(na(1),:)
  b3=cmt(na(3),:)-cmt(na(2),:)

  n1=crossprod(b1,b2)
  n2=crossprod(b2,b3)
  n1=n1/normvec(n1)
  n2=n2/normvec(n2)
  n3=crossprod(n1,n2)

  ax=dot_product(n3,b2)/normvec(b2)
  ay=dot_product(n1,n2)
  dha=datan2(ax,ay)

  if ( dha < 0.d0 ) dha=2.d0*pig+dha

  return

end subroutine dihedrang

!> bndang: subroutine bndang(cmt,natm,inda,na,bda)
!> dihedrang: Depends on crossprod, normvec
!> bndang: Computes the bond angle (bda) defined by the atom index inda and the first 2 atom indices contained in na
!> bndang: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given
subroutine bndang(cmt,natm,inda,na,bda)

  use vecop

  implicit none

  integer, intent(in) :: natm, inda
  double precision, intent(in) :: cmt(natm,3)
  integer, intent(in) :: na(3)
  double precision, intent(out) :: bda

  double precision u1(3), u2(3)
  double precision n1(3)
  double precision ax, ay
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  u1=cmt(inda,:)-cmt(na(1),:)
  u2=cmt(na(2),:)-cmt(na(1),:)

  n1=crossprod(u1,u2)
  ax=normvec(n1)
  ay=dot_product(u1,u2)

  bda=datan2(ax,ay)

  if ( bda < 0.d0 ) bda=2.d0*pig+bda

  return

end subroutine bndang

!> zmtline: subroutine zmtline(cmt,natm,inda,na1d,zmt1d)
!> zmtline: Depends on bndang, dihedrang
!> zmtline: Computes one row of the Z-matrix (zmt1d) for a specific atom index (inda)
!> zmtline: na1d is here a 1-dimensional array whose rows contain a sequence of 3 atom indices defining a bond, a bond angle or a torsion
!> zmtline: zmt1d is here a 1-dimensional array containing the values of the bond length, bond angle and dihedral angle defined by na1d.
!> zmtline: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given
    subroutine zmtline(cmt,natm,inda,na1d,zmt1d)

      implicit none

      integer, intent(in) :: natm
      double precision, intent(in) :: cmt(natm,3)
      integer, intent(in) :: inda, na1d(3)
      double precision, intent(out) :: zmt1d(3)

      integer i
      double precision pig, rad2deg
      parameter (pig = 4.d0*datan(1.d0), rad2deg=180.d0/pig)

      zmt1d=0.d0
      if ( na1d(1) /= 0 ) then
        do i=1, 3
         zmt1d(1)=zmt1d(1)+(cmt(inda,i)-cmt(na1d(1),i))**2
        enddo
        zmt1d(1)=dsqrt(zmt1d(1))
        if ( na1d(2) /= 0 ) then
         call bndang(cmt,natm,inda,na1d,zmt1d(2))
         zmt1d(2)=zmt1d(2)*rad2deg
         if ( na1d(3) /= 0 ) then
           call dihedrang(cmt,natm,inda,na1d,zmt1d(3))
           zmt1d(3)=zmt1d(3)*rad2deg
         endif
        endif
      endif

      return

    end subroutine zmtline

!> reordna: subroutine reordna(cmt,natm,na)
!> reordna: Evaluates the 2-dimensional array of sequences of atoms (na).
!> reordna: Working principles: For each atom (referenced by the row index i), the element na(i,1) is evaluated by considering the nearest atom previsouly defined in na. 
!> reordna: atom i is then supposed to be bounded to atom na(i,1). The two following indices, na(i,2) and na(i,3), are taken from the first and second elements of atom na(i,1).
!> reordna: The 2-dimensional array of cartesian coordinates (cmt) and the number of atoms (natm) must be given.
    subroutine reordna(cmt,natm,na)

      implicit none

      integer, intent(in) :: natm
      double precision, intent(in) :: cmt(natm,3)
      integer, intent(out) :: na(natm,3)

      integer i, j, indn
      double precision dist, distmin

      !% The first atom is temporarly defined such that the incremental evaluation of the following atoms is performed correctly
      na(1,:)=(/2, 3, 4/)
      do i=2, natm
        na(i,:)=(/2, 3, 4/)
        indn=0
        distmin=1.d5
        !% Finds the nearest atom to the atom i and whose index belongs to the range 1:i-1
        do j=1, i-1
          dist=(cmt(i,1)-cmt(j,1))**2+(cmt(i,2)-cmt(j,2))**2+(cmt(i,3)-cmt(j,3))**2
          if ( dist < distmin .and. na(j,1) /= j .and. na(j,2) /= j ) then
            distmin=dist
            indn=j
          endif
        enddo

        if ( indn == 0 ) then
          write(*,*)
          write(*,*)
          write(*,*) "Error!!! Couldn't find atom closer than distmin!"
          write(*,*)
          stop
        endif

        na(i,1)=indn
        !% Takes the first 2 elements of the nearest atom as the element 2 and element 3
        if ( i > 2 ) na(i,2)=na(indn,1)
        if ( i > 3 ) na(i,3)=na(indn,2)

      enddo
      
      !% The three first atoms are redefined correctly: atom1 (0,0,0), atom2 (1,0,0) and atom3 (1/2,2/1,0)
      na(1,:)=0
      na(2,2:3)=0
      na(3,3)=0

      return

    end subroutine reordna

!> convzmt2xyz: subroutine convzmt2xyz(zmt,cmt,na,natm)
!> convzmt2xyz: Converts a Z-matrix (2-dimensional arrays na and zmt) to cartesian coordinates (cmt)
!> convzmt2xyz: na is a 2-dimensional array whose rows contain sequences of 3 atom indices defining a bond, a bond angle or a torsion
!> convzmt2xyz: zmt is a 2-dimensional array containing the values of the bond length, bond angle and dihedral angle defined by na.
!> convzmt2xyz: cmt is a 2-dimensional array containing the cartesian coordinates of the atoms
!> convzmt2xyz: The number of atoms (natm) must be given
  subroutine convzmt2xyz(zmt,cmt,na,natm)

    implicit none

    integer, intent(in) :: natm
    double precision, intent(in) :: zmt(natm,3)
    integer, intent(in) :: na(natm,3)
    double precision, intent(out) :: cmt(natm,3)

    integer i
    double precision costmp, xyz(3)
    double precision pig, deg2rad

    parameter (pig = 4.d0*datan(1.d0), deg2rad=pig/180.d0)

    !% The cartesian coordinates of the first three atoms must be defined manually by adopting a convention
    !% We have adopted here the convention of Siesta: atom1 lies in the center, atom2 is located on the z-axis and atom3 in the x,z plane
    cmt(1,:)=0.d0
    cmt(2,1:2)=0.d0
    cmt(2,3)=zmt(2,1)
    costmp=dcos(zmt(3,2)*deg2rad)

    if ( na(3,1) == 1 ) then
       cmt(3,3)=cmt(1,3)+zmt(3,1)*costmp
    else
       cmt(3,3)=cmt(2,3)-zmt(3,1)*costmp
    endif
    cmt(3,1)=zmt(3,1)*dsin(zmt(3,2)*deg2rad)
    cmt(3,2)=0.d0

    do i=4, natm
      call xyzline(cmt,zmt(i,:),na(i,:),natm,xyz)
      cmt(i,:)=xyz
    enddo

    return
  end subroutine convzmt2xyz

!> xyzline: subroutine xyzline(cmt,zmt1d,na1d,natm,cmt1d)
!> xyzline: Depends on getnerfmt
!> xyzline: Computes one row of the array of cartesian coordinates (cmt1d) for a specific sequence of atom indices (na1d).
!> xyzline: The method used to compute efficiently the Z-matrix row is described in the article: Parsons et al, Journal of Comp. Chem., 26(10):1063-1068, 2005
!> xyzline: na1d is here a 1-dimensional array which contains a sequence of 3 atom indices defines the bond, the bond angle and the torsion
!> xyzline: zmt1d is here a 1-dimensional array containing the values of the bond length, bond angle and dihedral angle defined by na1d.
!> xyzline: cmt is a 2-dimensional array containing the cartesian coordinates of the atoms (at least up to the maximum atom index in na1d)
!> xyzline: The number of atoms (natm) must be given.
  subroutine xyzline(cmt,zmt1d,na1d,natm,cmt1d)

    implicit none

    integer, intent(in) :: natm
    double precision, intent(in) :: cmt(natm,3), zmt1d(3)
    integer, intent(in) :: na1d(3)
    double precision, intent(out) :: cmt1d(3)

    integer i
    double precision theta, phi
    double precision vecd(3)
    double precision nerfmt(3,3)
    double precision pig, deg2rad
    parameter (pig = 4.d0*datan(1.d0), deg2rad=pig/180.d0)

    theta=pig-zmt1d(2)*deg2rad
    phi=zmt1d(3)*deg2rad

    vecd(1)=zmt1d(1)*dcos(theta)
    vecd(2)=zmt1d(1)*dsin(theta)*dcos(phi)
    vecd(3)=zmt1d(1)*dsin(theta)*dsin(phi)
    
    !% Generates the Natural Extension Reference Frame from the vectors defined by the atoms in na1d
    call getnerfmt(cmt(na1d(3),:),cmt(na1d(2),:),cmt(na1d(1),:),nerfmt)
    
    !% Transforms this frame to the molecular frame and gets the cartesian coordinates of the atom
    cmt1d=matmul(nerfmt,vecd)+cmt(na1d(1),:)

  end subroutine xyzline

!> getnerfmt: subroutine getnerfmt(vec1,vec2,vec3,nerfmt)
!> getnerfmt: Depends on crossprod, normvec
!> getnerfmt: Generates the Natural Extension Reference Frame (nerfmt) from the vectors defined by the vectors vec1, vec2 and vec3 (see Parsons et al, Journal of Comp. Chem., 26(10):1063-1068, 2005)
  subroutine getnerfmt(vec1,vec2,vec3,nerfmt)

    use vecop

    implicit none

    double precision, intent(in) :: vec1(3),vec2(3),vec3(3)
    double precision, intent(out) :: nerfmt(3,3)

    double precision vectmp1(3), vectmp2(3)

    vectmp1=vec3-vec2
    nerfmt(:,1)=vectmp1/normvec(vectmp1)

    vectmp1=vec2-vec1
    vectmp2=crossprod(vectmp1,nerfmt(:,1))
    nerfmt(:,3)=vectmp2/normvec(vectmp2)

    nerfmt(:,2)=crossprod(nerfmt(:,3),nerfmt(:,1))

    return

  end subroutine getnerfmt
  

!> avgdiffcoo: subroutine avgdiffcoo(cmt1,cmt2,natm,avgdiff)
!> avgdiffcoo: Computes the total cartesian coordinates per atom (avgdiff) between the two geometries defined by the 2-dimensional array of cartesian coordinates (cmt1 and cmt2).
!> avgdiffcoo: The number of atoms (natm) must be given.
  subroutine avgdiffcoo(cmt1,cmt2,natm,avgdiff)
  
    implicit none
    
    integer, intent(in) :: natm
    double precision, intent(in) :: cmt1(natm,3), cmt2(natm,3)
    double precision, intent(out) :: avgdiff
    
    integer i, j
    
    avgdiff=0.d0
    do i=1, natm
      do j=1, 3
	avgdiff=avgdiff+dabs(cmt2(i,j)-cmt1(i,j))
      enddo
    enddo
    avgdiff=avgdiff/natm
    
    return
    
  end subroutine avgdiffcoo
  
!> maxdistcoo: subroutine maxdistcoo(cmt,natm,maxdist)
!> maxdistcoo: Computes the maximal possible distance (maxdist) between two spatial coordinates contained in the 2-dimensonal array of cartesian coordinates (cmt).
!> maxdistcoo: The number of atoms (natm) must be given.
  subroutine maxdistcoo(cmt,natm,maxdist)
  
    implicit none
    
    integer, intent(in) :: natm
    double precision, intent(in) :: cmt(natm,3)
    double precision, intent(out) :: maxdist
    
    integer i, j, k
    double precision dist
    
    maxdist=0.d0
    do i=1, natm-1
      do j=i+1, natm
	dist=0.d0
	do k=1, 3
	  dist=dist+(cmt(i,k)-cmt(j,k))**2
	enddo
	dist=dsqrt(dist)
	if ( dist > maxdist ) maxdist=dist
      enddo
    enddo
    
    return
    
  end subroutine maxdistcoo

!> initrandseed: subroutine initrandseed()
!> initrandseed: Initializes the seed by using the system time for random number generation
  subroutine initrandseed()
  
    implicit none
    
    integer i, n, clock
    integer, dimension(:), allocatable :: seed
    
    call random_seed(size=n)
    allocate(seed(n))
    call system_clock(count=clock)
    seed=clock+37*(/(i-1, i=1, n)/)
    call random_seed(put=seed)
    deallocate(seed)
    
    return
  
  end subroutine initrandseed

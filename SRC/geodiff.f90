!> geodiff: program geodiff
!> geodiff: Depends on numafile, getextension, checkext, numaxyzfile, numamultixyzfile, readxyzfile, readmultixyzfile, nearestat2, convxyz2zmt, readnamov, readtokfile2, zmodconv
!> geodiff: Computes and prints the maximal difference, the sum of the differences, the average difference and the standard deviation of the difference between the internal coordinates of two geometries
!> geodiff: read from two .xyz formatted files (xyzfile1 and xyzfile2). 
!> geodiff: If xyzfile2 contains multiple geometries, then the above mentionned differences will be computed for each couple of consecutive geometries
!> geodiff: and between the geometry of xyzfile1 and the final geometry of xyzfile2.
!> geodiff: Optionally, the differences can be calculated for specific bonds, bond angles or torsions provided by a file respectively formatted as .zmt, .boa and .toa
!> geodiff: A verbose option (verbose) controls the amount/type of information to be printed.
  program geodiff

    implicit none

    integer i, j, k, g, w, iuf, ok, wstep
    integer natm, nna, nlx, ngeo, verbose, zmode, imov
    integer imax(3), imaxmax(3)
    integer, dimension (:), allocatable :: alist
    integer, dimension (:,:), allocatable :: na0, amov
    character*2, dimension (:), allocatable :: atm1, atm2
    double precision, dimension (:,:), allocatable :: cmt1
    double precision, dimension (:,:,:), allocatable :: cmt2
    double precision, dimension (:,:), allocatable :: zmt0, zmt1, zmt2
    double precision, dimension (:,:), allocatable :: zdiff
    double precision, dimension (:), allocatable :: zdiffsumov
    double precision zdifftmp, zdiffmax(3), zdiffsum(3)
    double precision zdiffmaxmax(3), zdiffsumsum(3)
    double precision zdiffvar(3)
    character*20 wstr
    character*150 xyzfile1, xyzfile2, specfile, getextension
    character*20 FMT1, FMT2, FMT3, FMT4, FMT5, FMT6, FMT7
    parameter (FMT1='(i6,$)',FMT3='(i6,12f16.8)',FMT2='(f16.8,$)',FMT4='(a,3f16.8,a,3i6)',FMT5='(a,3f16.8)',FMT6='(i6,f16.8)')
    parameter (FMT7='(i6,a4,3f16.8)')
    double precision tol
    parameter (tol=1.d5)

      read(*,'(a)') xyzfile1
      read(*,'(a)') xyzfile2
      read(*,*) verbose
      zmode=0
      read(*,*,iostat=ok) specfile
      !% If the inputfile contains an additional line, reads the file with filename specfile containing the bonds, bond angles or torsions specifications and reads the option imov
      if ( ok == 0 .and.  specfile /= "" .and. specfile /= "none" ) then
	read(*,*) imov
	!% Attribute a specific mode depending on the extension of specfile
	select case (getextension(trim(specfile)))
	case("zmt")
	  zmode=1
	case("boa")
	  imov=2
	  zmode=2
	case("toa")
	  imov=3
	  zmode=3
	case default
	  write(*,*)
	  write(*,*) "Error!!! Bad extension for inputfile 3!"
	  write(*,*)
	  stop
	end select
	if ( imov < 1 .or. imov > 3 ) then
	  write(*,*)
	  write(*,*) "Error!!! Value of imov should be between 1 and 3: imov=", imov
	  write(*,*)
	  stop
	endif
      endif

    !% Checks the extension of xyzfile1 and xyzfile2, counts the number of atoms for each geometry (natm1 and nlx) and checks that they are equal
    call checkext(trim(xyzfile1),1,"xyz")
    call checkext(trim(xyzfile2),2,(/"xyz","XYZ"/))
    call numaxyzfile(trim(xyzfile1),natm)
    call numamultixyzfile(trim(xyzfile2),nlx,ngeo)
    if ( natm .ne. nlx ) then
      write(*,*)
      write(*,*) "Error!!! xyz files do not have the same number of atoms: natm, nlx=", natm, nlx
      write(*,*)
      stop
    endif

    allocate(cmt1(natm,3),atm1(natm))
    allocate(cmt2(ngeo,natm,3),atm2(natm))
    
    !% Reads and stores the elements of xyzfile1 and xyzfile2
    call readxyzfile(trim(xyzfile1),atm1,cmt1,natm)
    call readmultixyzfile(trim(xyzfile2),atm2,cmt2,natm,ngeo)
    !% Checks that atom labels (atm1 and atm2) are the same
    do i=1, natm
      if ( atm1(i) .ne. atm2(i) ) then
        write(*,*)
        write(*,*) "Error!!! Inconsistent list of atom labels: i, atm1(i), atm2(i)=", i, atm1(i), atm2(i)
        write(*,*)
        stop
      endif
    enddo

    if ( zmode == 0 ) then
      !% If no bonds, bond angles or torsions are specified, simply converts the cartesian cordinates of geometry1 (cmt1) in a Z-matrix (zmt0)
      nna=natm
      allocate(na0(nna,3),zmt0(nna,3))
      call convxyz2zmt(cmt1,natm,na0,zmt0,1)
      allocate(alist(nna))
      alist=[(i, i=1, nna)]

    else
      select case (zmode)
      !% If the specification file is a .zmt formatted file, reads and stores the sequence of atoms defining the bonds, bond angles and torsions in a 2-dimensional array (na0) and their corresponding freezing/release value in the 2-dimensional array amov and converts the cartesian cordinates of geometry1 (cmt1) in a Z-matrix (zmt0) according to na0
      case(1)
	call numafile(trim(specfile),nna)
	if ( natm .ne. nna ) then
	  write(*,*)
	  write(*,*) "Error!!! xyz and zmt files do not have the same number of atoms: natm, nna=", natm, nna
	  write(*,*)
	  stop
	endif
	allocate(na0(nna,3),zmt0(nna,3))
	allocate(amov(nna,3))
	call readnamov(trim(specfile),na0,amov,nna)
	!% Converts cmt1 to Z-matrix according to na0
	call convxyz2zmt(cmt1,natm,na0,zmt0,2)
	allocate(alist(nna))
	alist=[(i, i=1, nna)]

      case(2,3)
	!% If the specification file is a .boa or .toa formatted file, reads and stores the sequence of atoms defining the bonds, bond angles or torsions in a 2-dimensional array (na0) and set amov=1
	!% and converts the cartesian cordinates of geometry1 (cmt1) in a Z-matrix (zmt0) according to na0
	call numafile(trim(specfile),nna)
	allocate(na0(nna,3),zmt0(nna,3))
	allocate(alist(nna))
	call readtokfile2(trim(specfile),alist,na0,nna,zmode)
	allocate(amov(nna,3))
	amov=1
	!% Converts cmt1 to Z-matrix according to na0
	call zmodconv(cmt1,natm,alist,na0,nna,zmt0)

      end select

      nlx=sum(amov(:,imov))
      allocate(zdiffsumov(nlx))

    endif
    allocate(zmt1(nna,3),zmt2(nna,3))
    allocate(zdiff(nna,3))
    wstr="Total"

    if ( ngeo == 1 ) then
      wstep=1
    else
      wstep=ngeo-1
    endif
    !% Loop allowing to process 2 cases: the differences between each couple of consecutives geometries ; the differences between the initial geometry (from xyzfile1) and the final geometry (from xyzfile2) 
    do w=1, ngeo, wstep
      if ( zmode > 1 ) zdiffsumov=0.d0
      if ( w == ngeo ) wstr="First-last"
      !% Copy zmt0 to zmt1 so zmt0 remains the initial geometry (from xyzfile1)
      zmt1=zmt0

      imaxmax=0
      zdiffmaxmax=0.d0
      zdiffsumsum=0.d0
      
      !% Loop on the geometry index of the 3-dimensional array of cartesian coordinates cmt2
      do g=w, ngeo
	!% Converts cmt2 to Z-matrix (zmt2)
	if ( zmode > 1 ) then
	  call zmodconv(cmt2(g,:,:),natm,alist,na0,nna,zmt2)
	else
	  call convxyz2zmt(cmt2(g,:,:),natm,na0,zmt2,2)
	endif
	!% Prints some debug information
	do k=1, 3
	  if ( verbose == -1 ) then
	      write(*,*)
	      write(*,'(a,i1,a)') "Printing Z-matrix1_ind (", k, "):"
	  endif
	  do i=1, nna
	      if ( verbose == -1 ) then
		write(*,*) alist(i), (na0(i,j), j=1,3), zmt1(i,k), zmt2(i,k)
	      endif
	  enddo
	enddo
	!% Evaluates the maximal difference, the sum of the differences and the average difference between zmt1 and zmt2
	imax=0
	zdiffmax=0.d0
	zdiffsum=0.d0
	do i=1, nna
	  do j=1, 3
	    zdiff(i,j)=dabs(zmt1(i,j)-zmt2(i,j))
	    if ( j /= 1 ) then
	      zdifftmp=360.d0-zdiff(i,j)
	      if ( zdifftmp < zdiff(i,j) ) then
		if ( verbose == 1 ) then
		write(*,*) "Warning!!! Angles are closer when removing +2Pi. Taking the modulo:", &
		" iatm, id, zmt1(iatm,id), zmt2(iatm,id), zdiff(iatm,id)=", i, alist(i), j, zmt1(i,j), zmt2(i,j), zdifftmp
		endif
		zdiff(i,j)=zdifftmp
	      endif
	    endif
	    if ( zdiff(i,j) .gt. zdiffmax(j) ) then
	      zdiffmax(j)=zdiff(i,j)
	      imax(j)=alist(i)
	    endif
	    zdiffsum(j)=zdiffsum(j)+zdiff(i,j)
	  enddo
	enddo
	
	!% Computes the standard deviation
	zdiffvar=0.d0
	do i=1, nna
	  do j=1, 3
	    zdiffvar(j)=zdiffvar(j)+(1.d0/nna)*(zdiff(i,j)-zdiffsum(j)/nna)**2
	  enddo
	enddo
	zdiffvar=dsqrt(zdiffvar)

	!% Print the results, depending on the verbose option (verbose) and the choosen mode (zmode)
	if ( verbose == 4 ) then
            do i=1, nna
                write(*,FMT7) g, trim(atm1(i)), zdiff(i,:)
            enddo
	endif
	
	if ( verbose == 2 ) then
	    write(*,FMT3) g, zdiffmax, zdiffsum, zdiffsum/nna, zdiffvar
	endif

	if ( zmode > 0 ) then
	  if ( verbose == 3 ) then
	    write(*,FMT1) g
	    do i=1, nna
	      if ( amov(i,imov) == 1 ) write(*,FMT2) zdiff(i,imov)
	    enddo
	    write(*,*)
	  else
	    k=0
	    do i=1, nna
	      if ( amov(i,imov) == 1 ) then
		k=k+1
		zdiffsumov(k)=zdiffsumov(k)+zdiff(i,imov)
	      endif
	    enddo
	  endif
	endif

	do j=1, 3
	  if ( zdiffmax(j) .gt. zdiffmaxmax(j) ) then
	    zdiffmaxmax(j)=zdiffmax(j)
	    imaxmax(j)=imax(j)
	  endif
	enddo
	zdiffsumsum=zdiffsumsum+zdiffsum
	
	!% The compared Z-matrix (zmt2) becomes the referent one (zmt1) for the next iteration
	zmt1=zmt2

      enddo

      !% Print a summary of the differences
      if ( verbose == 0 ) then
	if ( nint(tol*zdiffmaxmax(1)) == 0 .and.  nint(tol*zdiffmaxmax(2)) == 0 .and. nint(tol*zdiffmaxmax(3)) == 0 ) then
	  write(*,*)
	  write(*,*) "0 The two geometries are most likely identical (+/-1.d-5)!"
	  write(*,*)
	else
	  write(*,*)
	  write(*,*) "1 The two geometries are different!"
	  write(*,*)
	endif
      endif
      if ( verbose == 1 ) then
	  write(*,*)
	  write(*,'(a,a)') trim(wstr), " Zmat differences:"
	  write(*,'(20X,a)') "     bond length      bond angle  dihedral angle"
	  write(*,FMT4) "Maximum difference: ", zdiffmaxmax, "      imax(1:3)= ", imaxmax
	  write(*,FMT5) "Sum of difference:  ", zdiffsumsum
	  write(*,FMT5) "Avg of difference:  ", zdiffsumsum/nna
	  write(*,FMT5) "Standard deviation: ", zdiffvar
	  write(*,*)
      endif

      !% Prints the evolution of the sum of the differences for each specified bond, bond angle or torsion
      if ( verbose == 4 .and. zmode > 0 ) then
	do i=1, nlx
	  write(*,FMT6) i, zdiffsumov(i)
	enddo
	write(*,*)
      endif

    enddo

    deallocate(cmt1,cmt2,atm1,atm2)
    deallocate(zmt0,zmt1,zmt2,na0)
    deallocate(alist)
    deallocate(zdiff)

    if ( zmode > 0 ) deallocate(amov,zdiffsumov)

  end


!> zmodconv: subroutine zmodconv(cmt,natm,alist,na,nna,zmt)
!> zmodconv: Depends on zmtline
!> zmodconv: Converts a 2-dimensional array of natm cartesian coordinates (cmt1) into a Z-matrix of nna elements according to the list of atoms (alist) and the corresponding sequence of 3 atoms (na)
!> zmodconv: defining the bond, the bond angles and the torsions.
    subroutine zmodconv(cmt,natm,alist,na,nna,zmt)

    implicit none

    integer, intent(in) :: natm, nna
    double precision, intent(in) :: cmt(natm,3)
    integer, intent(in) :: alist(nna), na(nna,3)
    double precision, intent(out) :: zmt(nna,3)

    integer i

    do i=1, nna
      call zmtline(cmt,natm,alist(i),na(i,:),zmt(i,:))
    enddo

    return

    end subroutine zmodconv

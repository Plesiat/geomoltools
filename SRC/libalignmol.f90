!> alignmol: subroutine alignmol(natm,cmt,nat1,ind1,cat0,nat2,ind2,align1,nat3,ind3,align2,verbose)
!> alignmol: Depends on ang2vec, vecrotperplan, convxyz2zmt, centroid, basepoint, rotcollinear, trans2point2, rotmatavec, genorthvec, checknummat
!> alignmol: Translates and aligns a geometry defined by 2-dimensional array of cartesian coordinates (cmt)
!> alignmol: ind1, ind2 and ind3 are three 1-dimensional arrays containing a set of atom indices defining three different centroids.
!> alignmol: If ind1, ind2 or ind3 has only one element which is equal or inferior to zero, then the corresponding geometry transformation is disabled
!> alignmol: The geometry is translated by a translation vector (cat0) and by taking the centroid1 (ind1) as reference.
!> alignmol: The geometry is then rotated (centroid1 being the center of rotation) by aligning the axis defined by centroid1-centroid2 with a first alignement vector (align1)
!> alignmol: The geometry is further rotated by aligning the axis defined by the altitude passing by centroid3 in (centroid1-centroid2-centroid3) with a second alignement vector (align2).
!> alignmol: The point defined by the intersection between the altitude and the axis centroid1-centroid2 constitues the center of rotation.
!> alignmol: A verbose option (verbose) controls the amount/type of information to be printed.
!> alignmol: The option rmode gives the possibility to define the 2 rotations by giving 2 alignement vectors va1(1:3)->align1 and va2(1:3)->align2 (rmode=0) 
!> alignmol: or by giving three angles in degree, one polar va1(1) and one azimuthal va1(2) for the first rotation, and one polar va2(1) for the second (internal) rotation (rmode=1).
!> alignmol: The number of atoms indices defined in ind1, ind2 and ind3 (nat1, nat2 and nat3, respectively) must be given.

subroutine alignmol(natm,cmt,nat1,ind1,cat0,nat2,ind2,va1,nat3,ind3,va2,verbose,rmode)

  use vecop

  implicit none

  integer, intent(in) :: natm, rmode
  integer, intent(in) :: nat1, nat2, nat3
  integer, intent(in) :: ind1(nat1), ind2(nat2), ind3(nat3)
  double precision, intent(in) :: cat0(3), va1(3), va2(3)
  integer, intent(in) :: verbose
  double precision, intent(inout) :: cmt(natm,3)

  integer i, j, k
  double precision align1(3), align2(3)
  double precision catC(3), catM(3), cat1(3), cat2(3), cat3(3)
  integer na1(natm,3), na2(natm,3)
  double precision zmt1(natm,3), zmt2(natm,3)
  character*15 FMT1, FMT2, FMT3
  parameter (FMT1='(a)',FMT2='(a,i5)',FMT3='(a,3f8.4)')
  
  !% Checks that the option for alignement/rotation mode (rmode) is one of the two values
  select case (rmode)
  case(0)
    align1=va1
    align2=va2
  case(1)
    align1=ang2vec(va1(1),va1(2),"degree")
  case default
    write(*,*)
    write(*,*) "Error!!! Bad argument for option rmode: rmode=", rmode
    write(*,*)
    stop
  end select

  !% The initial cartesian coordinates are first converted to Z-matrix in order to check (at the end) the final translated/aligned geometry
  if ( verbose > 0 ) then
    call convxyz2zmt(cmt,natm,na1,zmt1,1)
  endif


  !% We perform the first rotation (alignement of the axis centroid1-centroid2 with align1)
  if ( ind2(1) > 0 ) then
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* Rotating system (1):"
      write(*,FMT2) "  -> Referent atom(s):", (ind2(j), j=1,nat2)
      write(*,FMT3) "  -> Aligned to:", align1
    endif
    !% If translation is disabled, the geometry can be aligned directly (i.e., no center of rotation defined by centroid1)
    if ( ind1(1) > 0 ) then
      catC=0.d0
      catM=genorthvec(align1)
      !% The two first centroids are calculated
      call centroid(cmt,natm,ind1,nat1,cat1)
      call centroid(cmt,natm,ind2,nat2,cat2)
      !% If the axis centroid1-centroid2 is by any chances collinear with align1, we have to rotate the geometry
      call rotcollinear(cmt,natm,catC,catM,cat2,cat1,align1)
      !% The center of rotation is brought at the origine before rotating
      call trans2point2(cmt,natm,cat1,catC)
    endif
    !% The centroid2 needs to be recalculated because the the geometry may have been translated and rotated
    call centroid(cmt,natm,ind2,nat2,cat2)
    !% Rotation is performed
    call rotmatavec(cmt,natm,cat2,align1)
    if ( ind1(1) > 0 ) then
      !% The center of rotation is brought back to its original position
      call trans2point2(cmt,natm,catC,cat1)
    endif
  else
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* No primary rotation applied: option disabled."
    endif
  endif
  
  
  !% We perform the second rotation of the geometry around the axis centroid1-centroid2
  if ( ind3(1) > 0 .and. ind2(1) > 0 .and. ind1(1) > 0 ) then
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* Rotating system (2):"
      write(*,FMT2) "  -> Referent atom(s):", (ind3(j), j=1,nat3)
      if ( rmode == 0 ) then
	write(*,FMT3) "  -> Aligned to:", align2
      else
	write(*,FMT3) "  -> Rotated by:", va2(1)
      endif
    endif
    
    !% The three centroids and the basepoint are calculated
    call centroid(cmt,natm,ind1,nat1,cat1)
    call centroid(cmt,natm,ind2,nat2,cat2)
    call centroid(cmt,natm,ind3,nat3,cat3)
    call basepoint(cat1,cat3,cat2,catM)
    
    if ( rmode == 1 ) then
      !% When rmode=1 (i.e., when the angle phi2 is given instead of the vector align2) we need to calculate the alignement vector for the second rotation (align2)
      !% To do so, we evaluate the vector that belongs to a plane containing cat3-catM and perpendicular to align1 which is rotated by va1(2)-va2(1)=Phi2-Phi1 (clockwise)
      !% We have adopted the convention where Phi1 is substracted so cat3-catM is aligned in the same fashion even when Phi1 change when the geometry is initially oriented properly.
      !% Warning! The final alignement of cat3-catM depends also on the original orientation of cat3-catM with respect to align1.
      align2=vecrotperplan(align1,cat3-catM,va1(2)-va2(1),"degree")
    endif
    
    !% If the altitude is by any chances collinear with align2, we have to rotate the geometry
    call rotcollinear(cmt,natm,cat1,cat2,cat3,catM,align2)
    catC=0.d0
    !% The basepoint (which corresponds to the center of rotation) is brought at the origin before rotating
    call trans2point2(cmt,natm,catM,catC)
    
    !% The centroid3 needs to be recalculated because the the geometry has been translated (and maybe rotated)
    call centroid(cmt,natm,ind3,nat3,cat3)
    
    !% Rotation is performed
    call rotmatavec(cmt,natm,cat3,align2)
    !% The center of rotation is brought back to its original position
    call trans2point2(cmt,natm,catC,catM)
  else
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* No secondary rotation applied: option disabled."
    endif
  endif


  !% We finally perform the translation
  if ( ind1(1) > 0 ) then
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* Translating system:"
      write(*,FMT2) "  -> Referent atom(s):", (ind1(j), j=1,nat1)
      write(*,FMT3) "  -> Moved to:", (cat0(j), j=1,3)
    endif
    !% The first centroid is calculated
    call centroid(cmt,natm,ind1,nat1,cat1)
    !% Translation is performed
    call trans2point2(cmt,natm,cat1,cat0)
  else
    if ( verbose > 0 ) then
      write(*,*)
      write(*,FMT1) "* No translation applied: option disabled."
    endif
  endif

  !% Initial and final geometries in Z-matrix format are compared in order to check that the internal coordinates are preserved
  na2=na1
  if ( verbose > 0 ) then
    call convxyz2zmt(cmt,natm,na2,zmt2,2)
    call checknummat(zmt1,zmt2,natm,3,4)
  endif


end subroutine alignmol

!> rotcollinear: subroutine rotcollinear(cmt,natm,a1,a2,a3,a4,u1)
!> rotcollinear: Depends on crossprod, normvec, trans2point2, rotmatavec
!> rotcollinear: Checks if the vector u1(u1x,u1y,u1z) is collinear with the vector defined by the couple of cartesian coordinates (a3,a4).
!> rotcollinear: If collinear, the 2-dimensional array of cartesian coordinates (cmt) is rotated by Pi/2 around the axis defined by the couple of cartesian coordinates (a1,a2).
!> rotcollinear: The size of cmt (natm) must be given.
  subroutine rotcollinear(cmt,natm,a1,a2,a3,a4,u1)

    use vecop

    implicit none
    
    integer, intent(in) :: natm
    
    double precision, intent(in) :: a1(3), a2(3), a3(3), a4(3)
    double precision, intent(in) :: u1(3)
    double precision, intent(inout) :: cmt(natm)
    
    integer j
    double precision a5(3), ac(3)
    double precision u0(3), u2(3), u3(3)
    double precision tol
    parameter (tol=1.d6)

    do j=1, 3
      u2(j)=a2(j)-a1(j)
    enddo
    do j=1, 3
      u3(j)=a4(j)-a3(j)
    enddo

    u0=crossprod(u3,u1)
    !% If the vectors u3 (defined by the points a3, a4) and u1 are collinear, then we rotate the geometry by +/- Pi/2 around the axis u2 (defined by the points a1, a2) so u3 and u1 are perpendicular
    if ( nint(normvec(u0)*tol) == 0 ) then
      u0=crossprod(u2,u1)
      u0=u0/normvec(u0)
      ac=0.d0
      call trans2point2(cmt,natm,a4,ac)
      a5=a4-a3
      call rotmatavec(cmt,natm,a5,u0)
      call trans2point2(cmt,natm,ac,a4)
    endif

  end subroutine rotcollinear
  

!> basepoint: subroutine basepoint(a1,a2,a3,a4)
!> basepoint: Depends on normvec
!> basepoint: Calculate the cartesian coordinates (a4) of the point corresponding to the intersection between the altitude (a3,a4) and the base (a1,a2).
!> basepoint: a1, a2 and a3 are three 1-dimensional arrays containing the cartesian coordinates of three points defining the triangle a1a2a3.
  subroutine basepoint(a1,a2,a3,a4)
  
    use vecop

    implicit none
    
    double precision, intent(in) :: a1(3), a2(3), a3(3)
    double precision, intent(out) :: a4(3)

    integer i, j

    double precision vec(2,3), nvec(2)
    double precision num, costheta, midlength

    do j=1, 3
        vec(1,j)=a2(j)-a1(j)
    enddo
    do j=1, 3
        vec(2,j)=a3(j)-a1(j)
    enddo
    nvec=0.d0
    
    do i=1, 2
      nvec(i)=normvec(vec(i,:))
    enddo
    
    num=0.d0
    do j=1, 3
      num=num+vec(1,j)*vec(2,j)
    enddo
    costheta=num/(nvec(1)*nvec(2))
    midlength=nvec(1)*costheta

    do j=1, 3
      a4(j)=a1(j)+(a3(j)-a1(j))*midlength/nvec(2)
    enddo

  end subroutine basepoint

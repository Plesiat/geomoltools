!> mol2xyz: program mol2xyz
!> mol2xyz: Depends on getbasename, checkext, numamolfile, readmolfile, xyzbndreorder, writexyzfile
!> mol2xyz: Converts a .mol file (molfile) into a ordered .xyz file (xyzfile)
!> mol2xyz: The order of appearance of the atoms in the resulting .xyz file is defined by an initial atom index (inid) and the specification of the molecular bonds specified in the .mol file.
!> mol2xyz: The .xyz file is built hierarchically, starting from the newly defined initial atom and adding the bounded atoms to the previously defined ones.

  program mol2xyz

  implicit none

  integer i, j, k, ko, iuf
  integer natm, nbnd, inid
  character*2, dimension (:), allocatable :: atm
  integer, dimension (:), allocatable :: alist
  integer, dimension (:,:), allocatable :: bnd
  double precision, dimension (:,:), allocatable :: cmt
  double precision dist1, dist2

  character*150 molfile, xyzfile, getbasename
  
    read(*,'(a)') molfile
    read(*,*) inid

  !% Checks the extension of the .mol file
  call checkext(trim(molfile),1,"mol")
  xyzfile=trim(getbasename(molfile))//"_r.xyz"

  !% Reads the number of atoms (natm) and bonds (nbnd) and reads and stores the atom labels (atm), the cartesian coordinates (cmt) and the atom indices defining the bonds (bnd)
  call numamolfile(trim(molfile),natm,nbnd)
  allocate(cmt(natm,3),atm(natm),bnd(nbnd,3))
  call readmolfile(trim(molfile),cmt,atm,bnd,natm,nbnd)

  !% Give a list of atom indices (alist) containing the original order ot the atoms
  allocate(alist(natm))
  do i=1, natm
    alist(i)=i
  enddo
  if ( inid == 0 ) then
    inid=alist(1)
  endif

  !% Reorder atm and cmt according to inid and bnd
  call xyzbndreorder(cmt,atm,bnd,alist,inid,natm,nbnd)
  
  !% Writes the reorder atom abels (atm) and cartesian coordinates (cmt) in a new .xyz file
  call writexyzfile(trim(xyzfile),atm,cmt,natm)

  deallocate(cmt,atm,bnd)
  deallocate(alist)

  end

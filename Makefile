#=================================================================================
#=================================================================================
# Compiler? Possible values: ifort; gfortran
F90 = gfortran
#
# Optimize? Empty: default No optimization; 0: No Optimization; 1 Optimzation
OPT = 0
#=================================================================================


# Operating system, OS? automatic using uname: 
OS=$(shell uname)
#=================================================================================
#=================================================================================
# ifort compillation
#=================================================================================
ifeq ($(F90),ifort)
   # opt management
   ifeq ($(OPT),1)
      F90FLAGS = 
   else
      F90FLAGS = -O0 -check all -g -traceback
   endif
endif
#=================================================================================
#=================================================================================



#=================================================================================
#=================================================================================
# gfortran (osx and linux)
#=================================================================================
 ifeq ($(F90),gfortran)
   # opt management
   ifeq ($(OPT),1)
      F90FLAGS = 
   else
      F90FLAGS = -O0 -g -fbacktrace -fcheck=all -fwhole-file -fcheck=pointer -Wuninitialized
   endif
endif



#=================================================================================
#=================================================================================
$(info ***********************************************************************)
$(info ***********OS:           $(OS))
$(info ***********COMPILER:     $(F90))
$(info ***********OPTIMIZATION: $(OPT))
$(info ***********************************************************************)


F90_FLAGS = $(F90) $(F90FLAGS)
LYNK90 = $(F90_FLAGS)

LIBS := 
LYNKFLAGS = $(LIBS)

SRCDIR = SRC
BINDIR = BIN

source_lib1 = libmanmol.o
source_lib2 = libmanmol.o libalignmol.o
source_lib3 = libmanmol.o libjoinmol.o
source_lib4 = libmanmol.o libalignmol.o libjoinmol.o


# ===================================================================================
# main

all: mol2xyz pastemol movemol stackmol geodiff xyz2zmt_s zmt2xyz_s ucubcellgen

mol2xyz:  $(source_lib1)
	$(LYNK90) $(SRCDIR)/mol2xyz.f90 -o $(BINDIR)/mol2xyz.exe $(source_lib1) $(LYNKFLAGS)
	
pastemol: $(source_lib3)
	$(LYNK90) $(SRCDIR)/pastemol.f90 -o $(BINDIR)/pastemol.exe $(source_lib3) $(LYNKFLAGS)
	
movemol:  $(source_lib2)
	$(LYNK90) $(SRCDIR)/movemol.f90 -o $(BINDIR)/movemol.exe $(source_lib2) $(LYNKFLAGS)
	
stackmol: $(source_lib4)
	$(LYNK90) $(SRCDIR)/stackmol.f90 -o $(BINDIR)/stackmol.exe $(source_lib4) $(LYNKFLAGS)
	
geodiff: $(source_lib1)
	$(LYNK90) $(SRCDIR)/geodiff.f90 -o $(BINDIR)/geodiff.exe $(source_lib1) $(LYNKFLAGS)
	
xyz2zmt_s: $(source_lib1)
	$(LYNK90) $(SRCDIR)/xyz2zmt_s.f90 -o $(BINDIR)/xyz2zmt_s.exe $(source_lib1) $(LYNKFLAGS)
	
zmt2xyz_s: $(source_lib1)
	$(LYNK90) $(SRCDIR)/zmt2xyz_s.f90 -o $(BINDIR)/zmt2xyz_s.exe $(source_lib1) $(LYNKFLAGS)
	
ucubcellgen: $(source_lib1)
	$(LYNK90) $(SRCDIR)/ucubcellgen.f90 -o $(BINDIR)/ucubcellgen.exe $(source_lib1) $(LYNKFLAGS)



# ===================================================================================
# lib

libmanmol.o:
	$(F90_FLAGS) -c $(SRCDIR)/libmanmol.f90

libalignmol.o:
	$(F90_FLAGS) -c $(SRCDIR)/libalignmol.f90

libjoinmol.o:
	$(F90_FLAGS) -c $(SRCDIR)/libjoinmol.f90

#
#
#===============================================
#===============================================
clean: 
	rm -f *.o *.mod *.MOD *.mod *.MOD
	@echo "  done cleaning up"
very-clean: 
	rm -f *.o *.mod *.MOD *.mod *.MOD $(BINDIR)/*.exe
	@echo "  done cleaning up"
#===============================================
#===============================================
#



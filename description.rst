

####################
Description
####################

mol2xyz
_______

::
  
  Dependencies: getbasename, checkext, numamolfile, readmolfile, xyzbndreorder, writexyzfile

Converts a .mol file (molfile) into a ordered .xyz file (xyzfile).

The order of appearance of the atoms in the resulting .xyz file is defined by an initial atom index (inid) and the specification of the molecular bonds specified in the .mol file.

The .xyz file is built hierarchically, starting from the newly defined initial atom and adding the bounded atoms to the previously defined ones.


pastemol
________

::
  
  Dependencies: getbasename, checkext, numaxyzfile, readxyzfile, joinmol, writexyzfile

Gather the two geometries contained in the .xyz files xyzfile1 and xyzfile2 in a new single .xyz file with or without selection criteria ("modtrs" and "valtrs").



movemol
_______

::
  
  Dependencies: getbasename, checkext, numaxyzfile, readxyzfile, alignmol, writexyzfile

Translates and aligns a geometry defined by 2-dimensional array of cartesian coordinates (cmt) read from a .xyz formatted file (xyzfile) and writes the transformed geometry in a new .xyz file.

The paremeters defining the transformations are read from the input file.

"ind1", "ind2" and "ind3" are three 1-dimensional arrays containing a set of atom indices defining three different centroids.

The number of atoms indices defined in "ind1", "ind2" and "ind3" ("nat1", "nat2" and "nat3", respectively) must be previously given.

If ind1, ind2 or ind3 have only one element which is equal or inferior to zero, then the corresponding geometry transformation is disabled

The geometry is translated by a translation vector (cat0) and by taking the centroid1 (ind1) as reference.

The geometry is then rotated (centroid1 being the center of rotation) by aligning the axis defined by centroid1-centroid2 with a first alignement vector (align1).

The geometry is further rotated by aligning the axis defined by the altitude passing by centroid3 in (centroid1-centroid2-centroid3) with a second alignement vector (align2).

The point defined by the intersection between the altitude and the axis centroid1-centroid2 constitues the center of rotation.

The deletat option is used to delete some atoms used as markers (obsolete). If deletat=.true., then delete the atoms used as markers ; if deletat=.false., then do nothing.

A verbose option (verbose) controls the amount/type of information to be printed.



stackmol
________

::
  
  Dependencies: getbasename, ang2vec, checkext, numaxyzfile, readxyzfile, sequify, getgrid, nb2car_zpad, alignmol, joinmol, writexyzfile, writemultixyzfile, maxdistcoo, initrandseed, checkfile

Generates and write in the .xyz format a set of different geometries containing the cartesian coordinates of two geometries read from xyzfile1 and xyzfile2 and pasted together.

Each written .xyz file corresponds to a geometry with a particular orientation of geometry2 with respect to geometry1, depending on the process mode option (pmode) and its related parameters.

"pmode" can take the following values: "manual" and "random".

pmode="manual" corresponds to a mode in which the orientations of geometry2 are defined manually by a set of parameters contained in the inputfile.

pmode="random" corresponds to a mode in which the orientations of geometry2 are generated using a (pseudo-)random number generator and are selected according to parameters defined in the inputfile.



geodiff
_______

::
  
  Dependencies: numafile, getextension, checkext, numaxyzfile, numamultixyzfile, readxyzfile, readmultixyzfile, nearestat2, convxyz2zmt, readnamov, readtokfile2, zmodconv

Computes and prints the maximal difference, the sum of the differences, the average difference and the standard deviation of the difference between the internal coordinates of two geometries read from two .xyz formatted files (xyzfile1 and xyzfile2). 

If "xyzfile2" contains multiple geometries, then the above mentionned differences will be computed for each couple of consecutive geometries and between the geometry of xyzfile1 and the final geometry of "xyzfile2".

Optionally, the differences can be calculated for specific bonds, bond angles or torsions provided by a file respectively formatted as .zmt, .boa and .toa.

A verbose option (verbose) controls the amount/type of information to be printed.



xyz2zmt_s
_________

::
  
  Dependencies: getbasename, getextension, checkext, numaxyzfile, readxyzfile, uniqtab, convxyz2zmt, sortmat, reordna, zmtline, numafile, readtokfile, writezmtfile1, writezmtfile3

Converts the geometry defined by the cartesian coordinates contained in a .xyz formatted file (xyzfile) to a Z-matrix and writes it in a .zmt formatted file.

Sections (i.e., semi-independent block of atoms) in the Z-matrix can be define by setting a positive value to "ncut" or "nsplit".

"ncut" and "nsplit" correspond to the number of atom indices in "icut" and "isplit", each one defining where each section starts. 

The atoms following each of these atom indices will then be defined according only to the atoms in the section.

The value of nspecies determines if the Z-matrix will be stored in a standard format (nspecies=0) or in a format readable by Siesta (nspecies>0)

In the latter case, it is possible to give a set of atom indices (amov) for which we want the freezing/release options to be set to 1 (the default value being 1, i.e. frozen).

In the case of the atom indices icut, the freezing/release option will be setted up in such way that the "cutted" sections can move relatively to the previous section.

Optionally, it is possible to read a file (tokname) containing some bond, bond angles and torsions (specified in terms of atom indices) and  their respective force constants and to set up automatically the freezing/release options by defining an upper limit to the force constants (kthd) for each one of the internal coordinates.



zmt2xyz_s
_________

::
  
  Dependencies: checkext, getextension, getbasename, numafile, readzmtfile2, convzmt2xyz, writexyzfile

Converts a Z-matrix defined by the 1-dimensional array of atom indices na and the 2-dimensional array containing the values of the corresponding internal coordinates (zmt) read from a .zms or .zmt formatted file into a 2-dimensional array of cartesian coordinates (cmt) which is written in a .xyz formatted file.

If nspecies=0, the .zmt file is assumed to be written in a standard format where atom labels are given by characters.
If nspecies>0, the .zmt file is assumed to be written in a format readable by Siesta and the 1-dimensional array of atom labels is generated according to the order of the atom labels given in the inputfile



ucubcellgen
___________

::
  
  Dependencies: getbasename, getextension, numaxyzfile, readxyzfile, xyzline, numafile, readzmtfile2, convzmt2xyz, writexyzfile

Calculates and prints the vectors of the unit cell according to the bond length, the bond angle and the dihedral angle (values given in zmt1d) between 3 atoms defined in inda and an atom ind0 in the following cell.

The geometry of the molecule can be read in two formats: from a .xyz formatted file as cartesian coordinates or from a .zmt formatted file as a Z-matrix


####################
Readme
####################

About geomoltools
_________________

geomoltools is a set of 8 Fortran codes that can be used to easily manipulate molecular geometries:  

* mol2xyz: converts a .mol file into an ordered .xyz file
* pastemol: joins two .xyz files
* movemol: translates and aligns the molecule with some predefined axes
* stackmol: generates (manually or randomly) different stacking arrangements between two molecules
* geodiff: compares the internal coordinates of two molecules
* xyz2zmt_s: converts the cartesian coordinates contained in a .xyz file into Z-matrix (2 possible formats)
* zmt2xyz_s: converts a Z-matrix (from 2 possible formats) into cartesian coordinates
* ucubcellgen: calculates the vectors of a unit cell given some atomic coordinates

These codes use the librairies libmanmol, libalignmol and libjoinmol which contain modules, subroutines and functions that can be easily extracted in order to be integrated in other codes.


Getting started
_______________

To compile all the codes:

:: 

    make

  
To compile each code separately:

:: 

    make name-of-the-code


How to
______

For an overview of the functionalities of mol2xyz, pastemol, movemol, stackmol, geodiff, xyz2zmt_s, zmt2xyz_s and ucubcellgen, please go to the tutorial directory and have a look to the file tutorial.srt.

For a detailed explanation of the main programs, please have a look to the file description.srt.


Contact Information
___________________

e.plesiat@nanogune.eu

Licence
_______

The set of codes from geomoltools are made available under the GNU General Public License (GPL) version 2

